package com.example.inshopkotlin

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Debug
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.inshop.ui_sign_up.SignInActivity

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : AppCompatActivity() {
    @SuppressLint("InvalidAnalyticsName")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Analytics
        monitoringRamFirebase()

        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }

    /**
     * This method register the RAM and memory usage on the Analytics.
     */
    private fun monitoringRamFirebase(){
        // VM Heap Size
        val heapSize = Runtime.getRuntime().totalMemory()

        // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
        val heapMaxSize = Runtime.getRuntime().maxMemory()

        // Get amount of free memory within the heap in bytes. This size will increase // after garbage collection and decrease as new objects are created.
        val heapFreeSize = Runtime.getRuntime().freeMemory()

        // Allocated VM Memory
        val allocatedVM = Runtime.getRuntime().maxMemory() - Runtime.getRuntime().freeMemory()

        // Heap allocated
        val heapAllocated = Debug.getNativeHeapAllocatedSize()

        val analytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putLong("VM_Heap_size", heapSize)
        bundle.putLong("VM_Heap_max_size", heapMaxSize)
        bundle.putLong("Free_memory",heapFreeSize)
        bundle.putLong("Allocated_VM_Memory",allocatedVM)
        bundle.putLong("Heap_allocated", heapAllocated)
        analytics.logEvent("app_starts",bundle)
    }
}