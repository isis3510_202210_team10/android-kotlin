package database

import androidx.room.Database
import androidx.room.RoomDatabase
import database.dao.*
import database.entities.*

@Database(entities = [OrderShopEntity::class,AlertShopEntity::class, AlertDateShopEntity::class,SupplierShopEntity::class,ProductsSupplierShopEntity::class,ProductsShopEntity::class], version = 1)
abstract class InShopDatabase:RoomDatabase() {
    abstract fun getOrdersShopDao():OrderShopDao
    abstract fun getSupplierShopDao(): SupplierShopDao
    abstract fun getProductsSupplierShopDao(): ProductsSupplierShopDao
    abstract fun getProductsShopDao(): ProductsShopDao
    abstract fun getAlertsShopDao():AlertShopDao
    abstract fun getAlertDateShopDao():AlertShopDateDao
}