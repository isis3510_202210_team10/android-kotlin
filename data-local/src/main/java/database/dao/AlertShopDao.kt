package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.AlertShopEntity

@Dao
interface AlertShopDao {

    @Query("SELECT * FROM alertShop_table WHERE userId == :shopId")
    suspend fun getAllAlertsShop(shopId: String): List<AlertShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllAlertsShop(alertsShops: List<AlertShopEntity>)

    @Query("DELETE FROM orderShop_table")
    suspend fun clearAllAlertsShop()

}