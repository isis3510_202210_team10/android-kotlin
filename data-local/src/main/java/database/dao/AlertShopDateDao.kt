package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.AlertDateShopEntity
import database.entities.AlertShopEntity

@Dao
interface AlertShopDateDao {

    @Query("SELECT * FROM alertDateShop_table WHERE id == :shopId")
    suspend fun getDateAlertShop(shopId: String): AlertDateShopEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAlertDateShop(alertDateShop: AlertDateShopEntity)

    @Query("DELETE FROM alertDateShop_table")
    suspend fun clearAllAlerstDateShop()
}