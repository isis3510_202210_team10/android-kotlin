package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.OrderShopEntity

@Dao
interface OrderShopDao {

    @Query("SELECT * FROM orderShop_table WHERE shopId == :shopId")
    suspend fun getAllOrderShop(shopId: String): List<OrderShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllOrderShop(ordersShops: List<OrderShopEntity>)

    @Query("DELETE FROM orderShop_table")
    suspend fun clearAllOrderShop()

}