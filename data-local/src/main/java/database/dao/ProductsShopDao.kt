package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.ProductsShopEntity
import database.entities.ProductsSupplierShopEntity

@Dao
interface ProductsShopDao {

    @Query("SELECT * FROM products_Shop_table")
    suspend fun getAllProductsShop(): List<ProductsShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllProductsShop(ordersShops: List<ProductsShopEntity>)

    @Query("DELETE FROM products_Shop_table")
    suspend fun clearAllProductsShop()

}