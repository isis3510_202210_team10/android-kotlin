package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.ProductsSupplierShopEntity

@Dao
interface ProductsSupplierShopDao {

    @Query("SELECT * FROM products_supplierShop_table")
    suspend fun getAllProductsSupplierShop(): List<ProductsSupplierShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllProductsSupplierShop(ordersShops: List<ProductsSupplierShopEntity>)

    @Query("DELETE FROM products_supplierShop_table")
    suspend fun clearAllProductsSupplierShop()

}