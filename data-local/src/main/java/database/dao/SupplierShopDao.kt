package database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import database.entities.SupplierShopEntity

@Dao
interface SupplierShopDao {

    @Query("SELECT * FROM supplierShop_table")
    suspend fun getAllSupplierShop(): List<SupplierShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE )
    suspend fun insertAllSupplierShop(ordersShops: List<SupplierShopEntity>)

    @Query("DELETE FROM supplierShop_table")
    suspend fun clearAllSupplierShop()

}