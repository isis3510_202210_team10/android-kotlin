package database.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import database.InShopDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private const val INSHOP_DATABASE = "inshopDB"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) = Room.databaseBuilder(
        context, InShopDatabase::class.java, INSHOP_DATABASE
    ).build()

    @Singleton
    @Provides
    fun provideOrderShopDao(db:InShopDatabase) = db.getOrdersShopDao()

    @Singleton
    @Provides
    fun provideSupplierShopDao(db:InShopDatabase) = db.getSupplierShopDao()

    @Singleton
    @Provides
    fun provideProductsSupplierShopDao(db:InShopDatabase) = db.getProductsSupplierShopDao()

    @Singleton
    @Provides
    fun provideProductsShopDao(db:InShopDatabase) = db.getProductsShopDao()


    @Singleton
    @Provides
    fun provideAlertShopDao(db:InShopDatabase) = db.getAlertsShopDao()

    @Singleton
    @Provides
    fun provideAlertDateShopDao(db:InShopDatabase) = db.getAlertDateShopDao()
}