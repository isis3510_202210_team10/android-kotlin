package database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import database.dao.OrderShopDao

@Entity(tableName = "alertShop_table")
data class AlertShopEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id:String,
    @ColumnInfo(name = "date") val date:String,
    @ColumnInfo(name = "message") val message:String,
    @ColumnInfo(name = "type") val type:String,
    @ColumnInfo(name = "userId") val userId:String,
)