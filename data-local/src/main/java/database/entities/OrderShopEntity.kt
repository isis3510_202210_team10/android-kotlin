package database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import database.dao.OrderShopDao

@Entity(tableName = "orderShop_table")
data class OrderShopEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id:String,
    @ColumnInfo(name = "initialDate") val initialDate:String,
    @ColumnInfo(name = "arrivalDate") val arrivalDate:String,
    @ColumnInfo(name = "address") val address:String,
    @ColumnInfo(name = "status") val status:String,
    @ColumnInfo(name = "total") val total:Int,
    @ColumnInfo(name = "shopId") val shopId:String,
    @ColumnInfo(name = "supplierId") val supplierId:String
)