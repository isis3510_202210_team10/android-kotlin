package database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import database.dao.OrderShopDao

@Entity(tableName = "products_Shop_table")
data class ProductsShopEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id:String,
    @ColumnInfo(name = "name") val name:String,
    @ColumnInfo(name = "description") val description:String,
    @ColumnInfo(name = "type") val type:String,
    @ColumnInfo(name = "image") val image:String,
    @ColumnInfo(name = "expirationDate") val expirationDate:String,
    @ColumnInfo(name = "priceSupplier") val priceSupplier:Int,
    @ColumnInfo(name = "stock") val stock:Int,
    @ColumnInfo(name = "supplierId") val shopId:String
)