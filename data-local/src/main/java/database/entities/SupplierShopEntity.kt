package database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import database.dao.OrderShopDao

@Entity(tableName = "supplierShop_table")
data class SupplierShopEntity(
    @PrimaryKey
    @ColumnInfo(name = "id") val id:String,
    @ColumnInfo(name = "name") val name:String,
    @ColumnInfo(name = "email") val email:String,
    @ColumnInfo(name = "address") val address:String,
    @ColumnInfo(name = "phone") val phone:String,
    @ColumnInfo(name = "companyName") val companyName:String
)