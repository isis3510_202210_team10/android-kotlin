package com.example.data_remote.di.module

import com.example.data_remote.inshop.model.AlertShopApi
import com.example.data_remote.inshop.model.OrderShopApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AlertShopService @Inject constructor(
    private val api:InShopAPIClient
) {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun fetchAlertsShopById(shopId: String): List<AlertShopApi> {
        var resp = emptyList<AlertShopApi>()
        withContext(Dispatchers.IO) {
            try {
                val response = api.fetchAlertsShopById(shopId)
                if (response.isSuccessful) {
                    resp = response.body() ?: emptyList()
                } else {
                    resp = emptyList<AlertShopApi>()
                }
            } catch (error: Throwable) {
                resp = emptyList<AlertShopApi>()
            }
        }
        return resp
    }
}