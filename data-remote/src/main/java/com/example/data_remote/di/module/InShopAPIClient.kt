package com.example.data_remote.di.module

import com.example.data_remote.inshop.model.*
import com.inshop.ui_model.SupplierShop
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface InShopAPIClient {

    @GET("suppliers/")
    suspend fun fetchSuppliersShop(): Response<List<SupplierShopApi>>

    @GET("shops/{ShopId}/shopProducts")
    suspend fun fetchProductsByShopID(@Path("ShopId") shopId: String): Response<List<ProductsShopApi>>

    @GET("suppliers/{SupplierID}/productSuppliers")
    suspend fun fetchProductsSuppliersShopById(@Path("SupplierID") supplierId: String): Response<List<ProductsSupplierShopApi>>

    @GET("shops/{ShopId}/orders")
    suspend fun fetchOrdersShopById(@Path("ShopId") shopId: String): Response<List<OrderShopApi>>

    @GET("shops/{ShopId}/alerts")
    suspend fun fetchAlertsShopById(@Path("ShopId") shopId: String): Response<List<AlertShopApi>>

}