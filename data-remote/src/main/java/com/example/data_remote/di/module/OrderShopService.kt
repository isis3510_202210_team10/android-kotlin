package com.example.data_remote.di.module

import android.util.Log
import com.example.data_remote.inshop.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class OrderShopService @Inject constructor(
    private val api:InShopAPIClient
) {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun fetchOrdersShopById(shopId: String): List<OrderShopApi> {
        var resp = emptyList<OrderShopApi>()
        withContext(Dispatchers.IO) {
            try {
                val response = api.fetchOrdersShopById(shopId)
                if (response.isSuccessful) {
                    resp = response.body() ?: emptyList()
                } else {
                    resp = emptyList<OrderShopApi>()
                }
            } catch (error: Throwable) {
                resp = emptyList<OrderShopApi>()
            }
        }
        return resp
    }
}