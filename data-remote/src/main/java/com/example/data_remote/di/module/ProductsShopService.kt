package com.example.data_remote.di.module

import com.example.data_remote.inshop.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProductsShopService @Inject constructor(
    private val api:InShopAPIClient
) {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun fetchProductsShop(supplierId: String): List<ProductsShopApi> {
        var resp = emptyList<ProductsShopApi>()
        withContext(Dispatchers.IO) {
            try {
                val response = api.fetchProductsByShopID(supplierId)
                if (response.isSuccessful) {
                    resp = response.body() ?: emptyList()
                } else {
                    resp = emptyList<ProductsShopApi>()
                }
            } catch (error: Throwable) {
                resp = emptyList<ProductsShopApi>()
            }
        }
        return resp
    }
}