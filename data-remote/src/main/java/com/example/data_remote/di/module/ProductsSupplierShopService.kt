package com.example.data_remote.di.module

import com.example.data_remote.inshop.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProductsSupplierShopService @Inject constructor(
    private val api:InShopAPIClient
) {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun fetchSuppliersShop(supplierId: String): List<ProductsSupplierShopApi> {
        var resp = emptyList<ProductsSupplierShopApi>()
        withContext(Dispatchers.IO) {
            try {
                val response = api.fetchProductsSuppliersShopById(supplierId)
                if (response.isSuccessful) {
                    resp = response.body() ?: emptyList()
                } else {
                    resp = emptyList<ProductsSupplierShopApi>()
                }
            } catch (error: Throwable) {
                resp = emptyList<ProductsSupplierShopApi>()
            }
        }
        return resp
    }
}