package com.example.data_remote.di.module

import android.util.Log
import com.example.data_remote.inshop.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SupplierShopService @Inject constructor(
    private val api:InShopAPIClient
) {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun fetchSuppliersShop(shopId: String): List<SupplierShopApi> {
        var resp = emptyList<SupplierShopApi>()
        withContext(Dispatchers.IO) {
            try {
                val response = api.fetchSuppliersShop()
                if (response.isSuccessful) {
                    resp = response.body() ?: emptyList()
                } else {
                    resp = emptyList<SupplierShopApi>()
                }
            } catch (error: Throwable) {
                resp = emptyList<SupplierShopApi>()
            }
        }
        return resp
    }
}