package com.example.data_remote.inshop

import com.example.data_remote.inshop.model.OrderShopApi
import com.example.data_remote.inshop.model.OrderSupplier

interface InShopRemoteDataSource {
    suspend fun fetchOrdersShop(shopId:String): List<OrderShopApi>

    suspend fun fetchOrdersSupplier(supplierId:String): List<OrderSupplier>
}