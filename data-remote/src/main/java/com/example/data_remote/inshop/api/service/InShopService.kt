package com.example.data_remote.inshop.api.service

import com.example.data_remote.inshop.model.*
import com.inshop.ui_model.ProductSupplier
import com.inshop.ui_model.SupplierShop
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface InShopService {


    @FormUrlEncoded
    @POST("shops")
    fun createShop(
        @Field("name") name:String,
        @Field("email") email:String,
        @Field("address") address:String,
        @Field("phone") phone:String,
        @Field("type") type:String,
    ): Call<CreateResponse>

    @FormUrlEncoded
    @POST("suppliers")
    fun createSupplier(
        @Field("name") name:String,
        @Field("email") email:String,
        @Field("address") address:String,
        @Field("phone") phone:String,
        @Field("companyName") companyName:String,
    ):Call<CreateResponse>

    @GET("shops/email/{email}")
    fun getShopByEmail(@Path("email") email: String):Call<ShopResponse>

    @GET("suppliers/email/{email}")
    fun getSupplierByEmail(@Path("email") email: String):Call<SupplierResponse>

    @GET("shops/{ShopId}/orders")
    suspend fun fetchOrdersShopById(@Path("ShopId") shopId: String): Response<List<OrderShopApi>>

    @GET("suppliers/")
    suspend fun fetchSuppliers(): Response<List<SupplierShop>>

    @GET("suppliers/{SupplierID}/productSuppliers")
    suspend fun getProductsBySupplierID(@Path("SupplierID") supplierId: String): Response<List<ProductSupplier>>


    @GET("shops/{ShopId}/shopProducts")
    suspend fun getProductsByShopID(@Path("ShopId") shopId: String): Response<List<ShopProduct>>

}
