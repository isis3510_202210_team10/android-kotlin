package com.example.data_remote.inshop.model

data class AlertShopApi (
    val _id : String,
    val date : String,
    val message : String,
    val type: String,
    val userId: String,
)