package com.example.data_remote.inshop.model

data class CreateResponse(val acknowledged: Boolean, val insertedId:String)