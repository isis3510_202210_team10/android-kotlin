@file:Suppress("unused", "PropertyName", "SpellCheckingInspection")

package com.example.data_remote.inshop.model

/**
 * This file manage the GoogleMapDTO. It have all the classes and structures needed for retrieving the information.
 * @author Sergio Julian Zona Moreno
 */

/**
 * Structure from the map
 */
class GoogleMapDTO {
    var routes = ArrayList<Routes>()
}

/**
 * Structure from the routes
 */
class Routes {
    var legs = ArrayList<Legs>()
}

/**
 * Structure from the edges
 */

class Legs {
    var distance = Distance()
    var duration = Duration()
    var end_address = ""
    var start_address = ""
    var end_location =Location()
    var start_location = Location()
    var steps = ArrayList<Steps>()
}

/**
 * Structure from the steps (path to follow)
 */
class Steps {
    var distance = Distance()
    var duration = Duration()
    var end_address = ""
    var start_address = ""
    var end_location =Location()
    var start_location = Location()
    var polyline = PolyLine()
    var travel_mode = ""
    var maneuver = ""
}

/**
 * Duration from the trip.
 */
class Duration {
    var text = ""
    var value = 0
}

/**
 * Distance from the trip.
 */
class Distance {
    var text = ""
    var value = 0
}

/**
 * Points to draw in the poliline.
 */
class PolyLine {
    var points = ""
}

/**
 * Location from the site.
 */
class Location{
    var lat =""
    var lng =""
}