package com.example.data_remote.inshop.model

data class OrderShopApi(
    val _id : String,
    val initialDate : String,
    val arrivalDate : String,
    val address: String,
    val status: String,
    val total: Int,
    val shopId: String,
    val supplierId: String,
)
