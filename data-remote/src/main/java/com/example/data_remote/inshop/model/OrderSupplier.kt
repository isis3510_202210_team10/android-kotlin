package com.example.data_remote.inshop.model

data class OrderSupplier (
    val _id : String?,
    val initialDate : String?,
    val arrivalDate : String?,
    val address: String?,
    val total: Int?,
    val shopId: String?,
    val supplierId: String?,
)
