package com.example.data_remote.inshop.model

data class ProductsSupplierShopApi(
    val _id: String, val name:String,  val description:String,
    val type:String,  val image:String, val expirationDate:String,val priceSupplier:String,val stock:String,val supplierId:String
)
