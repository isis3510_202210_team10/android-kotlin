package com.example.data_remote.inshop.model

data class ShopProduct(
    val _id: String, val name:String,  val description:String,
    val type:String,  val image:String, val expirationDate:String,val priceSupplier:String,val stock:String,val shopId:String
)
