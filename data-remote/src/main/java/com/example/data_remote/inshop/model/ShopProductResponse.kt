package com.example.data_remote.inshop.model

import com.google.gson.annotations.SerializedName

data class ShopProductResponse(
    @SerializedName("status") var status: String ,
    @SerializedName("message") var images: List<String>
)
