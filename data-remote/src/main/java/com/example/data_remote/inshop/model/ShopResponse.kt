package com.example.data_remote.inshop.model

data class ShopResponse(val _id: String, val name:String,  val email:String,
                        val address:String,  val phone:String, val type:String)