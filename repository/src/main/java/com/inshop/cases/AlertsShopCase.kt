package com.inshop.cases

import android.annotation.SuppressLint
import com.inshop.repository.mappers.orders.toDataBase
import com.inshop.repository.alerts.AlertsRepository
import com.inshop.ui_model.AlertDateShop
import com.inshop.ui_model.AlertShop
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AlertsShopCase @Inject constructor(
    private val repository: AlertsRepository
) {
    @SuppressLint("SimpleDateFormat")
    suspend operator fun invoke(
        shopId: String,
        isConnected: Boolean
    ): Pair<List<AlertShop>, AlertDateShop> {

        return if (isConnected) {
            val orderShop = repository.getAlertsShopFromApi(shopId)
            if (orderShop.isNotEmpty()) {
                repository.clearAlertsShop()
                repository.insertAlertsShop(orderShop.map {
                    it.toDataBase()
                })
            }
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm a")
            val currentDateAndTime: String = simpleDateFormat.format(Date())
            val date = AlertDateShop(shopId,currentDateAndTime)
            repository.clearAllAlertsDateShop()
            repository.insertAlertDateShop(date)
            Pair(orderShop, date)
        } else {
            Pair(
                repository.getAlertsShopFromDatabase(shopId),
                repository.getAlertDateShopFromDatabase(shopId)
            )
        }
    }

    suspend fun deleteAlertsShop() {
        repository.clearAlertsShop()
    }

    suspend fun deleteAlertsDateShop() {
        repository.clearAllAlertsDateShop()
    }

}