package com.inshop.cases

import com.inshop.repository.mappers.orders.toDataBase
import com.inshop.repository.orders.OrdersRepository
import com.inshop.ui_model.OrderShop
import javax.inject.Inject

class OrdersShopCase @Inject constructor(
    private val repository : OrdersRepository
) {
    suspend operator fun invoke(shopId: String, isConnected: Boolean): List<OrderShop> {

        return if(isConnected) {
            val orderShop = repository.getOrderShopFromApi(shopId)
            if (orderShop.isNotEmpty()) {
                repository.clearOrderShop(shopId)
                repository.insertOrderShop(orderShop.map {
                    it.toDataBase()
                })
            }
                orderShop
        } else {
            repository.getOrderShopFromDatabase(shopId)
        }
    }

    suspend fun delete() {
        repository.deleteOrderShop()
    }
}