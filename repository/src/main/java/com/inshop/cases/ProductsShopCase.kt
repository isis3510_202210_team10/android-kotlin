package com.inshop.cases

import com.inshop.repository.mappers.orders.toDataBase
import com.inshop.repository.productsShop.ProductsRepository
import com.inshop.repository.productsSupplier.ProductsSuppliersRepository
import com.inshop.ui_model.ProductSupplierShop
import com.inshop.ui_model.ShopProduct
import javax.inject.Inject

class ProductsShopCase @Inject constructor(
    private val repository : ProductsRepository
) {
    suspend operator fun invoke(shopId: String, isConnected: Boolean): List<ShopProduct> {

        return if(isConnected) {
            val orderShop = repository.getProductsShopFromApi(shopId)
            if (orderShop.isNotEmpty()) {
                repository.clearProductsSupplierShop(shopId)
                repository.insertProductsShop(orderShop.map {
                    it.toDataBase()
                })
            }
                orderShop
        } else {
            repository.getProductsShopFromDatabase()
        }
    }

    suspend fun delete() {
        repository.deleteProductsSupplierShop()
    }
}