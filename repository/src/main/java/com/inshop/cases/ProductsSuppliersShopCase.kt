package com.inshop.cases

import com.inshop.repository.mappers.orders.toDataBase
import com.inshop.repository.productsSupplier.ProductsSuppliersRepository
import com.inshop.ui_model.ProductSupplierShop
import javax.inject.Inject

class ProductsSuppliersShopCase @Inject constructor(
    private val repository : ProductsSuppliersRepository
) {
    suspend operator fun invoke(shopId: String, isConnected: Boolean): List<ProductSupplierShop> {

        return if(isConnected) {
            val orderShop = repository.getProductsSupplierShopFromApi(shopId)
            if (orderShop.isNotEmpty()) {
                repository.clearProductsSupplierShop(shopId)
                repository.insertProductsSupplierShop(orderShop.map {
                    it.toDataBase()
                })
            }
                orderShop
        } else {
            repository.getProductsSupplierShopFromDatabase()
        }
    }

    suspend fun delete() {
        repository.deleteProductsSupplierShop()
    }
}