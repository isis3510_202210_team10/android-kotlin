package com.inshop.cases

import com.inshop.repository.mappers.orders.toDataBase
import com.inshop.repository.suppliers.SuppliersRepository
import com.inshop.ui_model.SupplierShop

import javax.inject.Inject

class SuppliersShopCase @Inject constructor(
    private val repository : SuppliersRepository
) {
    suspend operator fun invoke(shopId: String, isConnected: Boolean): List<SupplierShop> {

        return if(isConnected) {
            val orderShop = repository.getSupplierShopFromApi(shopId)
            if (orderShop.isNotEmpty()) {
                repository.clearSupplierShop(shopId)
                repository.insertSupplierShop(orderShop.map {
                    it.toDataBase()
                })
            }
                orderShop
        } else {
            repository.getSupplierShopFromDatabase()
        }
    }

    suspend fun delete() {
        repository.deleteSupplierShop()
    }
}