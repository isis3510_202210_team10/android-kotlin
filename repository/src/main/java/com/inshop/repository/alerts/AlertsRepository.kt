package com.inshop.repository.alerts

import com.example.data_remote.di.module.AlertShopService
import com.inshop.repository.mappers.orders.toDatabase
import com.inshop.repository.mappers.orders.toDomain
import com.inshop.ui_model.AlertDateShop
import com.inshop.ui_model.AlertShop
import database.dao.AlertShopDao
import database.dao.AlertShopDateDao
import database.entities.AlertShopEntity
import javax.inject.Inject

class AlertsRepository @Inject constructor(
    private val api: AlertShopService,
    private val alertShopDao: AlertShopDao,
    private val alertDateShopDao: AlertShopDateDao
)
{
    suspend fun getAlertsShopFromApi(shopId: String): List<AlertShop> {
        val response = api.fetchAlertsShopById(shopId)
        return response.map { it.toDomain() }
    }

    suspend fun getAlertsShopFromDatabase(shopId: String): List<AlertShop> {
        val response = alertShopDao.getAllAlertsShop(shopId)
        return response.map { it.toDomain() }
    }

    suspend fun insertAlertsShop(orderShop: List<AlertShopEntity>) {
        alertShopDao.insertAllAlertsShop(orderShop)
    }

    suspend fun clearAlertsShop() {
        alertShopDao.clearAllAlertsShop()
    }

    suspend fun getAlertDateShopFromDatabase(shopId: String): AlertDateShop {
        val response = alertDateShopDao.getDateAlertShop(shopId)
        return if(response != null)
            response.toDomain()
        else
            AlertDateShop("", "")
    }

    suspend fun insertAlertDateShop(date:AlertDateShop) {
        alertDateShopDao.insertAlertDateShop(date.toDatabase())
    }

    suspend fun clearAllAlertsDateShop(){
        alertDateShopDao.clearAllAlerstDateShop()
    }
}