package com.inshop.repository.mappers.orders
import com.example.data_remote.inshop.model.AlertShopApi
import com.inshop.ui_model.AlertDateShop
import com.inshop.ui_model.AlertShop
import database.entities.AlertDateShopEntity
import database.entities.AlertShopEntity

class AlertsShopMapper

fun AlertShopApi.toDomain() = AlertShop(
    _id, date, message, type, userId
)

fun AlertShopEntity.toDomain() = AlertShop(
    id, date, message, type, userId
)

fun AlertShop.toDataBase() = AlertShopEntity(
    id, date, message, type, userId
)

fun AlertDateShopEntity.toDomain() = AlertDateShop(
    id, date
)

fun AlertDateShop.toDatabase() = AlertDateShopEntity(
    id, date
)