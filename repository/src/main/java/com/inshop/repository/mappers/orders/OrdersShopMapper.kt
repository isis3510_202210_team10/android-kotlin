package com.inshop.repository.mappers.orders
import com.example.data_remote.inshop.model.OrderShopApi
import com.inshop.ui_model.OrderShop
import database.entities.OrderShopEntity

class OrdersShopMapper

fun OrderShopApi.toDomain() = OrderShop(
    _id, initialDate, arrivalDate, address, status, total, shopId, supplierId
)

fun OrderShopEntity.toDomain() = OrderShop(
    id, initialDate, arrivalDate, address, status, total, shopId, supplierId
)

fun OrderShop.toDataBase() = OrderShopEntity(
    id, initialDate, arrivalDate, address, status, total, shopId, supplierId
)