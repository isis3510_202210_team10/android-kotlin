package com.inshop.repository.mappers.orders
import com.example.data_remote.inshop.model.ProductsShopApi

import com.inshop.ui_model.ShopProduct
import database.entities.ProductsShopEntity


class ProductsShopMapper

fun ProductsShopApi.toDomain() = ShopProduct(
    _id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),shopId
)

fun ProductsShopEntity.toDomain() = ShopProduct(
    id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),shopId
)

fun ShopProduct.toDataBase() = ProductsShopEntity(
    _id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),shopId
)