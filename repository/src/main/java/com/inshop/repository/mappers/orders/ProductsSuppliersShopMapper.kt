package com.inshop.repository.mappers.orders
import com.example.data_remote.inshop.model.OrderShopApi
import com.example.data_remote.inshop.model.ProductsSupplierShopApi
import com.example.data_remote.inshop.model.SupplierShopApi
import com.inshop.ui_model.ProductSupplierShop
import com.inshop.ui_model.SupplierShop
import database.entities.ProductsSupplierShopEntity
import database.entities.SupplierShopEntity

class ProductsSuppliersShopMapper

fun ProductsSupplierShopApi.toDomain() = ProductSupplierShop(
    _id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),supplierId
)

fun ProductsSupplierShopEntity.toDomain() = ProductSupplierShop(
    id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),supplierId
)

fun ProductSupplierShop.toDataBase() = ProductsSupplierShopEntity(
    _id, name, description, type, image, expirationDate,priceSupplier.toInt(),stock.toInt(),supplierId
)