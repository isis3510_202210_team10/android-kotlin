package com.inshop.repository.mappers.orders
import com.example.data_remote.inshop.model.OrderShopApi
import com.example.data_remote.inshop.model.SupplierShopApi
import com.inshop.ui_model.SupplierShop
import database.entities.SupplierShopEntity

class SuppliersShopMapper

fun SupplierShopApi.toDomain() = SupplierShop(
    _id, name, email, address, phone, companyName
)

fun SupplierShopEntity.toDomain() = SupplierShop(
    id, name, email, address, phone, companyName
)

fun SupplierShop.toDataBase() = SupplierShopEntity(
    _id, name, email, address, phone, companyName
)