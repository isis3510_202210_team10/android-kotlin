package com.inshop.repository.orders

import com.example.data_remote.di.module.OrderShopService
import com.inshop.repository.mappers.orders.toDomain
import com.inshop.ui_model.OrderShop
import database.dao.OrderShopDao
import database.entities.OrderShopEntity
import javax.inject.Inject

class OrdersRepository @Inject constructor(
    private val api:OrderShopService,
    private val orderShopDao: OrderShopDao
)
{
    suspend fun getOrderShopFromApi(shopId: String): List<OrderShop> {
        val response = api.fetchOrdersShopById(shopId)
        return response.map { it.toDomain() }
    }

    suspend fun getOrderShopFromDatabase(shopId: String): List<OrderShop> {
        val response = orderShopDao.getAllOrderShop(shopId)
        return response.map { it.toDomain() }
    }

    suspend fun insertOrderShop(orderShop: List<OrderShopEntity>) {
        orderShopDao.insertAllOrderShop(orderShop)
    }

    suspend fun clearOrderShop(shopId: String) {
        orderShopDao.clearAllOrderShop()
    }

    suspend fun deleteOrderShop(){
        orderShopDao.clearAllOrderShop()
    }
}