package com.inshop.repository.productsShop

import com.example.data_remote.di.module.ProductsShopService
import com.inshop.repository.mappers.orders.toDomain
import com.inshop.ui_model.ShopProduct
import database.dao.ProductsShopDao
import database.entities.ProductsShopEntity
import javax.inject.Inject

class ProductsRepository @Inject constructor(
    private val api: ProductsShopService,
    private val productssupplierShopDao: ProductsShopDao
)
{
    suspend fun getProductsShopFromApi(supplierId: String): List<ShopProduct> {
        val response = api.fetchProductsShop(supplierId)
        return response.map { it.toDomain() }
    }

    suspend fun getProductsShopFromDatabase(): List<ShopProduct> {
        val response = productssupplierShopDao.getAllProductsShop()
        return response.map { it.toDomain() }
    }

    suspend fun insertProductsShop(supplierShop: List<ProductsShopEntity>) {
        productssupplierShopDao.insertAllProductsShop(supplierShop)
    }

    suspend fun clearProductsSupplierShop(supplierId: String) {
        productssupplierShopDao.clearAllProductsShop()
    }

    suspend fun deleteProductsSupplierShop(){
        productssupplierShopDao.clearAllProductsShop()
    }
}