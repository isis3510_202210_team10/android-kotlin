package com.inshop.repository.productsSupplier

import com.example.data_remote.di.module.ProductsSupplierShopService
import com.inshop.repository.mappers.orders.toDomain
import com.inshop.ui_model.ProductSupplierShop
import database.dao.ProductsSupplierShopDao
import database.entities.ProductsSupplierShopEntity
import javax.inject.Inject

class ProductsSuppliersRepository @Inject constructor(
    private val api: ProductsSupplierShopService,
    private val productssupplierShopDao: ProductsSupplierShopDao
)
{
    suspend fun getProductsSupplierShopFromApi(supplierId: String): List<ProductSupplierShop> {
        val response = api.fetchSuppliersShop(supplierId)
        return response.map { it.toDomain() }
    }

    suspend fun getProductsSupplierShopFromDatabase(): List<ProductSupplierShop> {
        val response = productssupplierShopDao.getAllProductsSupplierShop()
        return response.map { it.toDomain() }
    }

    suspend fun insertProductsSupplierShop(supplierShop: List<ProductsSupplierShopEntity>) {
        productssupplierShopDao.insertAllProductsSupplierShop(supplierShop)
    }

    suspend fun clearProductsSupplierShop(supplierId: String) {
        productssupplierShopDao.clearAllProductsSupplierShop()
    }

    suspend fun deleteProductsSupplierShop(){
        productssupplierShopDao.clearAllProductsSupplierShop()
    }
}