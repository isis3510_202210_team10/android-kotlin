package com.inshop.repository.suppliers

import com.example.data_remote.di.module.SupplierShopService
import com.inshop.repository.mappers.orders.toDomain
import com.inshop.ui_model.SupplierShop
import database.dao.SupplierShopDao
import database.entities.SupplierShopEntity
import javax.inject.Inject

class SuppliersRepository @Inject constructor(
    private val api: SupplierShopService,
    private val supplierShopDao: SupplierShopDao
)
{
    suspend fun getSupplierShopFromApi(supplierId: String): List<SupplierShop> {
        val response = api.fetchSuppliersShop(supplierId)
        return response.map { it.toDomain() }
    }

    suspend fun getSupplierShopFromDatabase(): List<SupplierShop> {
        val response = supplierShopDao.getAllSupplierShop()
        return response.map { it.toDomain() }
    }

    suspend fun insertSupplierShop(supplierShop: List<SupplierShopEntity>) {
        supplierShopDao.insertAllSupplierShop(supplierShop)
    }

    suspend fun clearSupplierShop(supplierId: String) {
        supplierShopDao.clearAllSupplierShop()
    }

    suspend fun deleteSupplierShop(){
        supplierShopDao.clearAllSupplierShop()
    }
}