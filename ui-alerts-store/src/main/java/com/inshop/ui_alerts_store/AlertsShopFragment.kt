package com.inshop.ui_alerts_store

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inshop.network_connection.NetworkConnection
import com.example.ui_alerts_store.databinding.FragmentAlertsStoreBinding
import com.inshop.ui_alerts_store.alerts.AlertsShopAdapter
import com.inshop.ui_model.AlertShop
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AlertsShopFragment : Fragment() {
    private var _binding: FragmentAlertsStoreBinding? = null

    private lateinit var adapter: AlertsShopAdapter
    private val alertsShop = mutableListOf<AlertShop>()
    private val alertsShopViewModel: AlertsShopViewModel by viewModels()

    private val binding get() = _binding!!

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAlertsStoreBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initRecyclerview()

        alertsShopViewModel.alertsShopModel.observe(viewLifecycleOwner, Observer {
            alertsShop.clear()
            adapter.notifyDataSetChanged()
            if(it.isNotEmpty()) {
                binding.fetchStatus.isVisible = false
                it.map { alert ->
                    alertsShop.add(alert) }

                binding.viewLoading.isVisible = false
                binding.viewOrders.isVisible = true

            } else {
                binding.viewLoading.isVisible = false
                binding.fetchStatus.isVisible = true
            }
        })

        alertsShopViewModel.alertsDateShopModel.observe(viewLifecycleOwner, Observer {
            if(it.date.isEmpty()){
                binding.date.isVisible = false
                binding.alertsTextDate.isVisible = true
                binding.alertsTextDate.text = "No date yet"
                binding.fetchStatus.isVisible = true
            } else {
                binding.date.isVisible = false
                binding.alertsTextDate.isVisible = true
                binding.alertsTextDate.text = "Update at: " + it.date
                binding.fetchStatus.isVisible = false
            }
        })

        NetworkConnection.getInstance(container!!.context).observe(viewLifecycleOwner) { isConnected ->
            binding.networkMessage.isVisible = !isConnected
            if(isConnected){
                binding.viewLoading.isVisible = true
                binding.date.isVisible = true
                binding.viewOrders.isVisible = false
                binding.alertsTextDate.isVisible = false
                binding.fetchStatus.isVisible = false
                getAlerts(container)
            }
        }

        getAlerts(container)
        return root
    }

    private fun initRecyclerview() {
        adapter = AlertsShopAdapter(alertsShop)
        val recyclerView = binding.orderShopRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = adapter
    }

    private fun getAlerts(container: ViewGroup) {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        alertsShopViewModel.getAlerts(idShop, isConnected)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { _, _ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}

