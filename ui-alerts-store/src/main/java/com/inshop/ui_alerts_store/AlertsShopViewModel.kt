package com.inshop.ui_alerts_store

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.inshop.cases.AlertsShopCase
import com.inshop.ui_model.AlertDateShop
import com.inshop.ui_model.AlertShop
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AlertsShopViewModel @Inject constructor(
    private val getAlertsShop: AlertsShopCase,
) : ViewModel() {

    val alertsShopModel = MutableLiveData<List<AlertShop>>()
    val alertsDateShopModel = MutableLiveData<AlertDateShop>()

    fun getAlerts(idShop: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            val alertsList = mutableListOf<AlertShop>()
            val (response, date) = getAlertsShop.invoke(idShop, isConnected)
            alertsList.addAll(response)
            alertsDateShopModel.postValue(date)
            alertsShopModel.postValue(alertsList)
        }
    }

    fun deleted() {
        viewModelScope.launch {
            getAlertsShop.deleteAlertsShop()
            getAlertsShop.deleteAlertsDateShop()
        }
    }
}