package com.inshop.ui_alerts_store.alerts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ui_alerts_store.R
import com.inshop.ui_model.AlertShop

class AlertsShopAdapter(private val alertsShop: List<AlertShop>): RecyclerView.Adapter<AlertsShopView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlertsShopView {
        val layoutInflater = LayoutInflater.from(parent.context)
        return AlertsShopView(layoutInflater.inflate(R.layout.item_alertshop, parent, false))
    }

    override fun onBindViewHolder(holder: AlertsShopView, position: Int) {
        val item = alertsShop[position]
        val context: Context = holder.itemView.context
        holder.render(item, context)
    }

    override fun getItemCount(): Int = alertsShop.size
}