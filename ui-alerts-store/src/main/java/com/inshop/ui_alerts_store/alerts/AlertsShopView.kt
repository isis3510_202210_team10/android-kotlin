package com.inshop.ui_alerts_store.alerts

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ui_alerts_store.R
import com.inshop.ui_model.AlertShop

class AlertsShopView(view: View): RecyclerView.ViewHolder(view) {

    val date = view.findViewById<TextView>(R.id.order_item_text_Date)
    val PO = view.findViewById<TextView>(R.id.order_item_Date)

    fun render(alertShop: AlertShop, context: Context) {
        date.text = alertShop.message
        PO.text = alertShop.date
    }
}