package com.inshop.inventory

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inshop.inventory.databinding.FragmentInventoryBinding
import com.inshop.inventory.products.ShopProductAdapter
import com.inshop.network_connection.NetworkConnection
import com.inshop.ui_model.ShopProduct
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class InventoryFragment : Fragment(),View.OnClickListener {

    private var _binding: FragmentInventoryBinding? = null
    private lateinit var adapter: ShopProductAdapter
    private val products = mutableListOf<ShopProduct>()
    private val shopProductViewModel: InventoryViewModel by viewModels()

    var navc: NavController?= null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentInventoryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        shopProductViewModel.productsShop.observe(viewLifecycleOwner, Observer {
            products.clear()
            it.map { product -> products.add(product) }
            if(it.isNotEmpty()) {
                initRecyclerview()
            } else {
                showAlert("List of Products", "The List of Orders is Empty")
            }

        })

        NetworkConnection.getInstance(container!!.context).observe(viewLifecycleOwner) { isConnected ->
            binding.networkMessage.isVisible = !isConnected
            if(isConnected) {
                getProducts(container)
            }
        }

        getProducts(container)

        val homeViewModel =
            ViewModelProvider(this).get(InventoryViewModel::class.java)

        val textView: TextView = binding.textView
        val textView2: TextView = binding.textView2
        val textView3: TextView = binding.totalStock
        val textView4: TextView = binding.totalPrice
        val simpleDateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm ")
        val currentDateAndTime: String = simpleDateFormat.format(Date())
        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = "Products"
            textView2.text=" Last Update: " + currentDateAndTime.toString()
            textView3.text=" Total Stock: 0 "
            textView4.text = "Total Price: 0 COP"

        }
        return root
    }
/*    override fun onStart() {
        super.onStart()
        initRecyclerview()
        getProducts()
    }*/

    private fun initRecyclerview() {
        adapter = ShopProductAdapter(products)
        val recyclerView = binding.shopProductRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = adapter
    }

    private fun getProducts(container: ViewGroup)
    {
            val sharedPreference =  activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
            val idShop = sharedPreference.getString("ID", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            shopProductViewModel.getProducts(idShop,isConnected)
    }

    fun calculateStock()
    {
        var totalStock: Int = 0
        var foodStock: Int = 0
        var cleaningStock: Int = 0
        var technologyStock: Int = 0
        var clothingStock: Int = 0
        var stationaryStock: Int = 0

        for (product in products)
        {
            totalStock += product.stock
            if (product.type=="FOOD")
            {
                foodStock += product.stock
            }
            else if(product.type=="CLEANING" )
            {
                cleaningStock += product.stock
            }else if(product.type=="CLOTING" )
            {
                clothingStock += product.stock
            }else if(product.type=="TECHNOLOGY" )
            {
                technologyStock += product.stock
            }else if(product.type=="STATIONERY" )
            {
                stationaryStock += product.stock
            }
        }

        val homeViewModel =
            ViewModelProvider(this).get(InventoryViewModel::class.java)

        val txtTotalStock: TextView = binding.totalStock
        val txtfoodStock: TextView = binding.fooStock
        val txtCleaningStock: TextView = binding.cleaningStock
        val txtClothingStock: TextView = binding.clothingStock
        val txtStationeryStock: TextView = binding.statStock
        val txtTechnologyStock: TextView = binding.techStock
        homeViewModel.text.observe(viewLifecycleOwner) {
            txtTotalStock.text = "Total Stock: "+ totalStock.toString()

            txtfoodStock.text = "Food Stock: "+ foodStock.toString()
            txtCleaningStock.text = "Cleaning Stock: "+ cleaningStock.toString()
            txtClothingStock.text = "Clothing Stock: "+ clothingStock.toString()
            txtStationeryStock.text = "Stationary Stock: "+ stationaryStock.toString()
            txtTechnologyStock.text = "Technology Stock: "+ technologyStock.toString()
        }
    }

    fun calculateTotalPrice()
    {
        var totalPrice: Int = 0
        var foodPrice: Int = 0
        var cleaningPrice: Int = 0
        var technologyPrice: Int = 0
        var clothingPrice: Int = 0
        var stationaryPrice: Int = 0

        for (product in products)
        {
            totalPrice += product.priceSupplier
            if (product.type=="FOOD")
            {
                foodPrice += product.priceSupplier
            }
            else if(product.type=="CLEANING" )
            {
                cleaningPrice += product.priceSupplier
            }else if(product.type=="CLOTING" )
            {
                clothingPrice += product.priceSupplier
            }else if(product.type=="TECHNOLOGY" )
            {
                technologyPrice += product.priceSupplier
            }else if(product.type=="STATIONERY" )
            {
                stationaryPrice += product.priceSupplier
            }
        }

        val homeViewModel =
            ViewModelProvider(this).get(InventoryViewModel::class.java)

        val txtTotalPrice: TextView = binding.totalPrice
        val txtfoodPrice: TextView = binding.foodPrice
        val txtCleaningPrice: TextView = binding.cleaningPrice
        val txtClothingPrice: TextView = binding.clothingPrice
        val txtStationeryPrice: TextView = binding.statPrice
        val txtTechnologyPrice: TextView = binding.techPrice
        homeViewModel.text.observe(viewLifecycleOwner) {
            txtTotalPrice.text = "Total Price: "+ totalPrice.toString()+" COP"

            txtfoodPrice.text = "Food Stock: "+ foodPrice.toString()+" COP"
            txtCleaningPrice.text = "Cleaning Price: "+ cleaningPrice.toString()+" COP"
            txtClothingPrice.text = "Clothing Price: "+ clothingPrice.toString()+" COP"
            txtStationeryPrice.text = "Stationary Price: "+ stationaryPrice.toString()+" COP"
            txtTechnologyPrice.text = "Technology Price: "+ technologyPrice.toString()+" COP"
        }

    }


    fun calculateExpirationProducts()
    {

        val productsExpiredNear = mutableListOf<ShopProduct>()
        val dates = SimpleDateFormat("yyyy-MM-dd")
        var currentDate: String
        var finalDate: String
        var date1: Date
        var date2: Date
        var diff: Long
        var difference: Long
        var differenceDates: Double

        for (product in products)
        {

            currentDate = dates.format(Date())
            finalDate = product.expirationDate
            //showAlert("Final y cURRENT",finalDate+"     "+currentDate)
            date1 = dates.parse(currentDate)
            date2 = dates.parse(finalDate)

            diff = date1.time - date2.time
            difference = kotlin.math.abs(diff)
            differenceDates = (difference / (24 * 60 * 60 * 1000)).toDouble()
            //val dayDifference = differenceDates.toString()

            if(differenceDates < 7.0)
            {
                productsExpiredNear.add(product)
            }

        }

        //listView = view?.findViewById<ListView>(R.id.lista_productos_expirados)
        var listItems = ""
// 3
        for (i in 0 until productsExpiredNear.size) {
            val product = products[i]
            listItems += "Product with name: "+ product.name + ", of Type: "+ product.type + ", with stock of: "+ product.stock+ " \n"
        }

        if(listItems=="")
        {
            listItems= "No one product has an expiration date nearly (Less than a week) , so very nice :D ."
        }

        showAlert("Products with near expiration (less than one week)",listItems)

    }


    /**
     * Alert to notify the user from a message.
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String){
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok"){ _,_ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

/*    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navc= Navigation.findNavController(view)

        view.findViewById<Button>(R.id.calculate_stock)?.setOnClickListener {
            calculateStock()

        }

        view.findViewById<Button>(R.id.calculate_price)?.setOnClickListener {
            calculateTotalPrice()
        }
        view.findViewById<Button>(R.id.calculate_expiration_products)?.setOnClickListener {
            calculateExpirationProducts()
        }

    }

    override fun onClick(p0: View?) {
        TODO("Not yet implemented")
    }
}