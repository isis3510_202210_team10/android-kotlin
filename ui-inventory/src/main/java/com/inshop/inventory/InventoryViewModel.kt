package com.inshop.inventory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.inshop.cases.ProductsShopCase
import com.inshop.cases.ProductsSuppliersShopCase
import com.inshop.ui_model.ProductSupplierShop
import com.inshop.ui_model.ShopProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InventoryViewModel @Inject constructor(
    private val getProductsShop: ProductsShopCase,
) : ViewModel() {

    val productsShop = MutableLiveData<List<ShopProduct>>()

//    fun getProducts(idShop: String) {
//        viewModelScope.launch(Dispatchers.IO) {
//
//            val productsList = mutableListOf<ShopProduct>()
//            val instance = RetrofitClient.INSTANCE.getProductsByShopID(idShop)
//            val response = instance.body()
//            if (instance.isSuccessful) {
//                for (product in response!!) {
//                    val productUI = ShopProduct(
//                        product._id, product.name,
//                        product.description,
//                        product.type, product.image, product.expirationDate,
//                        product.priceSupplier.toInt(),
//                        product.stock.toInt(),
//                        product.shopId
//                    )
//                    productsList.add(productUI)
//                }
//                productsShop.postValue(productsList)
//            } else {
//                productsShop.postValue(emptyList<ShopProduct>())
//            }
//        }
//    }

    fun getProducts(idShop: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {

            val productsList = mutableListOf<ShopProduct>()
            val response = getProductsShop.invoke(idShop, isConnected)

            productsList.addAll(response)
            productsShop.postValue(productsList)

        }
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text



}