package com.inshop.inventory.products

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.ShopProduct
import com.inshop.inventory.R

class ShopProductAdapter(private val productsList: MutableList<ShopProduct>): RecyclerView.Adapter<ShopProductsView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopProductsView {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ShopProductsView(layoutInflater.inflate(R.layout.item_productshop, parent, false))
    }

    override fun onBindViewHolder(holder: ShopProductsView, position: Int) {
        val item = productsList[position]
        holder.render(item)
    }

    override fun getItemCount(): Int = productsList.size
}