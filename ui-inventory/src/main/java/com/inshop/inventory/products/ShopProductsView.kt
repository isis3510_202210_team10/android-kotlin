package com.inshop.inventory.products

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.ShopProduct
import com.inshop.inventory.R

class ShopProductsView(view: View): RecyclerView.ViewHolder(view) {

    val idProduct = view.findViewById<TextView>(R.id.id_product)
    val nameProduct = view.findViewById<TextView>(R.id.name_product)
    val typeProduct = view.findViewById<TextView>(R.id.type_product)
    val expiProduct = view.findViewById<TextView>(R.id.expiration_date_product)
    val priceProduct = view.findViewById<TextView>(R.id.price_product)
    val stockProduct = view.findViewById<TextView>(R.id.stock_product)

    fun render(productShop : ShopProduct) {
        idProduct.text = productShop._id.substring(0,4)
        nameProduct.text = productShop.name
        typeProduct.text= productShop.type
        expiProduct.text=productShop.expirationDate
        priceProduct.text=productShop.priceSupplier.toString()
        stockProduct.text=productShop.stock.toString()
    }
}