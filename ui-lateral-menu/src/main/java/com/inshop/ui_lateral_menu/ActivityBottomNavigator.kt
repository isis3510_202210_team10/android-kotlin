package com.inshop.ui_lateral_menu

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.inshop.ui_lateral_menu.databinding.ActivityBottomNavigatorBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityBottomNavigator : AppCompatActivity() {

    private lateinit var binding: ActivityBottomNavigatorBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val activityBottomViewModel: ActivityBottomViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!this::binding.isInitialized) {
            binding = ActivityBottomNavigatorBinding.inflate(layoutInflater)
        }

        //Activate top bar
        supportActionBar?.show()
        setContentView(binding.root) // Because we are working with fragments, we should use binding.root.

        navController = findNavController(R.id.nav_host_fragment_activity_bottom_navigator)
        //bottom nav
        binding.navView.setupWithNavController(navController)
        //drawer
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_configuration,
                R.id.navigation_inventory,
                R.id.navigation_stats,
                R.id.navigation_store
            ),
            binding.drawerLayout
        )

        //menu item click handle
        binding.navView.setupWithNavController(navController)
        setupActionBarWithNavController(navController, appBarConfiguration)

        val idEnter: String = intent.getStringExtra("id").toString()
        Log.d("ActivityBottomNavigator", idEnter.toString())
        val nameEnter: String = intent.getStringExtra("name").toString()

        val userType: String = intent.getStringExtra("userType").toString()

    }

    override fun onDestroy() {
        super.onDestroy()
        activityBottomViewModel.deleteLocalStorage()
    }

    //bottom nav
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController) ||
                super.onOptionsItemSelected(item)
    }

    //open drawer when drawer icon clicked and back btn pressed
    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment_activity_bottom_navigator).navigateUp(
            appBarConfiguration
        )
    }
}