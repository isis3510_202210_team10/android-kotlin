package com.inshop.ui_lateral_menu

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.inshop.cases.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ActivityBottomViewModel @Inject constructor(
    private val getAlertsShop: AlertsShopCase,
    private val getOrdersShop: OrdersShopCase,
    private val getSuppliersShop: SuppliersShopCase,
    private val getProductsSuppliersShop: ProductsSuppliersShopCase,
    private val getProductsShop: ProductsShopCase,

    ): ViewModel() {

    fun deleteLocalStorage() {
        viewModelScope.launch(Dispatchers.IO) {
            getAlertsShop.deleteAlertsShop()
            getAlertsShop.deleteAlertsDateShop()
            getOrdersShop.delete()
            getSuppliersShop.delete()
            getProductsSuppliersShop.delete()
            getProductsShop.delete()
        }
    }
}