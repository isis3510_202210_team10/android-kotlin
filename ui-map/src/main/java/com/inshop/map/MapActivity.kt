package com.inshop.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.*
import android.os.Bundle
import android.os.Debug
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.BOTTOM
import androidx.constraintlayout.widget.ConstraintSet.TOP
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.data_remote.inshop.model.GoogleMapDTO
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.inshop.network_connection.NetworkConnection
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * This class manage the Map interaction. Get an order direction, an show the optimal Route between the current
 * position and the selected destination.
 * @author Sergio Julian Zona Moreno
 */
@Suppress("SpellCheckingInspection")
class MapActivity : AppCompatActivity(), OnMapReadyCallback {
    /**
     * Map attributes
     */
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var googleMap: GoogleMap

    /**
     * Location attributes
     */
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var currentLoc: LatLng? = null
    private var destinationLoc: LatLng? = null
    private lateinit var destinationName: String

    companion object {
        const val REQUEST_CODE_LOCATION = 0
    }

    /**
     * UI attributes
     */
    private var collapsed = false

    /**
     * Constructor of the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreference =  getSharedPreferences("PREFERENCE_INSHOP_ORDER", Context.MODE_PRIVATE)!!
        destinationName = sharedPreference.getString("DESTINATION_ADDRESS", "Default")!!

        // Eventual connectivity
        val networkConnection = NetworkConnection(applicationContext)
        networkConnection.observe(this) { isConnected ->
            if (isConnected) {
                setContentView(R.layout.activity_map)

                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

                createFragment()
                monitoringRamFirebase()
            } else {
                setContentView(R.layout.no_connection)
            }
        }
    }

    /**
     * Create the map Fragment
     */
    private fun createFragment() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Deploy the map with the locations
     */
    @SuppressLint("SetTextI18n")
    override fun onMapReady(pGoogleMap: GoogleMap) {
        googleMap = pGoogleMap
        enableLocation()
        getLocationsAndDraw()
    }

    /**
     * Create and animate the places card
     */
    @SuppressLint("SetTextI18n")
    private fun createPlacesCard() {
        // Set places card values
        val placesConstraint = findViewById<ConstraintLayout>(R.id.constraint_places_card)
        val txtPlaces = findViewById<TextView>(R.id.places_txt)
        val btnShowRoute = findViewById<Button>(R.id.show_route_btn)
        val txtInitialLocation =
            findViewById<TextInputEditText>(R.id.initial_direction_text_input_txt)
        val txtFinalLocation = findViewById<TextInputEditText>(R.id.final_direction_text_input_txt)
        val txtDistance = findViewById<TextView>(R.id.distance_txt)
        txtInitialLocation.setText("Current location")
        txtFinalLocation.setText(destinationName)
        txtDistance.text = "DISTANCE: Calculating..."

        // Animate the camera when clicking
        btnShowRoute?.setOnClickListener {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLoc!!, 15f))
        }

        // Animate the icon
        val icono = findViewById<ImageButton>(R.id.imageButton)
        icono.setOnClickListener {
            if (!collapsed) {
                txtPlaces.visibility = View.GONE
                txtInitialLocation.visibility = View.GONE
                txtFinalLocation.visibility = View.GONE
                btnShowRoute.visibility = View.GONE
                icono.setImageResource(android.R.drawable.arrow_down_float)

                val set = ConstraintSet()
                set.clone(placesConstraint)
                set.connect(txtDistance.id, TOP, placesConstraint.id, TOP)
                set.connect(txtDistance.id, BOTTOM, placesConstraint.id, BOTTOM)
                set.applyTo(placesConstraint)

                collapsed = true
            } else {
                txtPlaces.visibility = View.VISIBLE
                txtInitialLocation.visibility = View.VISIBLE
                txtFinalLocation.visibility = View.VISIBLE
                btnShowRoute.visibility = View.VISIBLE
                icono.setImageResource(android.R.drawable.arrow_up_float)

                // Example for understading
                // Declare the set and select the layout which contains the element/s.
                val set = ConstraintSet()
                set.clone(placesConstraint)

                // The following breaks the connection.
                set.clear(txtDistance.id, BOTTOM)

                // This part connect the constraints
                set.connect(
                    txtDistance.id, // Element that is going to connect
                    TOP, // Which constraint
                    final_direction_text_input.id, // To which element will it connect
                    BOTTOM, // To which constraint will connect
                    8.toDp(this) // margin (optional)
                )
                set.applyTo(placesConstraint)

                collapsed = false
            }
        }
    }

    /**
     * Get the current location
     */
    @SuppressLint("MissingPermission")
    private fun getLocationsAndDraw() {
        CoroutineScope(Dispatchers.Default).launch {
            val task = fusedLocationProviderClient.lastLocation
            runOnUiThread {
                task.addOnSuccessListener {
                    if (it != null) {
                        // Current location
                        currentLoc = LatLng(it.latitude, it.longitude)
                        addLocation(currentLoc!!, "My location")

                        // Destination location
                        destinationLoc = getLocationFromAddress(destinationName)
                        addLocation(destinationLoc!!, destinationName)

                        // URLs and routes.
                        val url = getDirectionURL(currentLoc!!, destinationLoc!!)
                        getDirectionGoogleApi(url)

                        createPlacesCard() // Creates the places card on the Thread.
                    }
                }
            }

        }
    }

    /**
     * Verifies if there are permission to get location.
     */
    private fun isLocationPermissionGranted() = ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED

    /**
     * Enabled the current location
     */
    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        if (!::googleMap.isInitialized) return
        if (isLocationPermissionGranted()) {
            googleMap.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }

    /**
     * Verify if the user gives permission to the app.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return
                }
                googleMap.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    this,
                    "Please give location permissions in settings to the application",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {}
        }
    }

    /**
     * Second verification of the permissions
     */
    override fun onResumeFragments() {
        super.onResumeFragments()
        if (!::googleMap.isInitialized) return
        if (!isLocationPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            googleMap.isMyLocationEnabled = false
            Toast.makeText(
                this,
                "To activate the location, please go to settings and give permissions to the app",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    /**
     * Request the user to accept the location permission.
     */
    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                this,
                "Please give location permissions in settings to the application",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
        }
    }

    /**
     * Add a marker to the location by Lat-Lng
     */
    private fun addLocation(location: LatLng, title: String) {
        googleMap.addMarker(MarkerOptions().position(location).title(title))
    }

    /**
     * Get Lat-Lng from Address
     */
    private fun getLocationFromAddress(strAddress: String): LatLng? {
        val coder = Geocoder(this)
        val address: List<Address>
        val location: LatLng
        try {
            address = coder.getFromLocationName(strAddress, 5)
            val coordenates: Address = address[0]
            location = LatLng((coordenates.latitude), (coordenates.longitude))
            return location
        } catch (e: Exception) {

        }
        return null
    }

    /**
     * URL to get the directions between two points
     */
    private fun getDirectionURL(origin: LatLng, dest: LatLng): String {
        return "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${dest.latitude},${dest.longitude}&sensor=false&mode=driving&key=AIzaSyBWAgnrBzD9jZFhK8n6Cun1PEbMFo-oytA"
    }

    /**
     * This method returns the route between two points. It uses a JAVA thread for retrieving the information.
     */
    private fun getDirectionGoogleApi(url: String) {
        Thread {
            val client = OkHttpClient()
            val request = Request.Builder().url(url).build()
            val response = client.newCall(request).execute()
            val data = response.body!!.string()

            runOnUiThread {
                Log.d("Test", data)
                val result = ArrayList<List<LatLng>>()
                try {
                    val respObj = Gson().fromJson(data, GoogleMapDTO::class.java)
                    val path = ArrayList<LatLng>()

                    // Get the distance between the locations
                    val distanceValue = (respObj.routes[0].legs[0].distance.value).toDouble() / 1000
                    val distance = "DISTANCE: " + String.format("%.2f", distanceValue) + " km"
                    val txtDistance = findViewById<TextView>(R.id.distance_txt)
                    txtDistance.text = distance

                    // Get the path between the locations
                    for (i in 0 until respObj.routes[0].legs[0].steps.size) {
                        path.addAll(decodePolyline(respObj.routes[0].legs[0].steps[i].polyline.points))
                    }
                    result.add(path)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val lineoption = PolylineOptions()
                for (i in result.indices) {
                    lineoption.addAll(result[i])
                    lineoption.width(10f)
                    lineoption.color(Color.RED)
                    lineoption.geodesic(true)
                }
                googleMap.addPolyline(lineoption)
            }
        }.start()
    }

    /**
     * Decode the format that comes in the JSON.
     */
    private fun decodePolyline(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].code - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val latLng = LatLng((lat.toDouble() / 1E5), (lng.toDouble() / 1E5))
            poly.add(latLng)
        }
        return poly
    }

    /**
     * This method allows to set values in DP
     */
    private fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
    ).toInt()

    /**
     * This method register the RAM and memory usage on the Analytics.
     */
    private fun monitoringRamFirebase(){
        // VM Heap Size
        val heapSize = Runtime.getRuntime().totalMemory()

        // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
        val heapMaxSize = Runtime.getRuntime().maxMemory()

        // Get amount of free memory within the heap in bytes. This size will increase // after garbage collection and decrease as new objects are created.
        val heapFreeSize = Runtime.getRuntime().freeMemory()

        // Allocated VM Memory
        val allocatedVM = Runtime.getRuntime().maxMemory() - Runtime.getRuntime().freeMemory()

        // Heap allocated
        val heapAllocated = Debug.getNativeHeapAllocatedSize()

        val analytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putLong("VM_Heap_size", heapSize)
        bundle.putLong("VM_Heap_max_size", heapMaxSize)
        bundle.putLong("Free_memory",heapFreeSize)
        bundle.putLong("Allocated_VM_Memory",allocatedVM)
        bundle.putLong("Heap_allocated", heapAllocated)
        analytics.logEvent("map_render",bundle)
    }
}