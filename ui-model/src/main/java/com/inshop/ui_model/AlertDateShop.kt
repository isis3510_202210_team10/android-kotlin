package com.inshop.ui_model

data class AlertDateShop (
    val id : String,
    val date : String,
) {
    companion object {
        val EMPTY = AlertDateShop(
            "",
            ""
        )
    }
}