package com.inshop.ui_model

data class AlertShop (
    val id : String,
    val date : String,
    val message : String,
    val type: String,
    val userId: String,
)