package com.inshop.ui_model

data class OrderShop (
    val id : String,
    val initialDate : String,
    val arrivalDate : String,
    val address: String,
    val status: String,
    val total: Int,
    val shopId: String,
    val supplierId: String,
)