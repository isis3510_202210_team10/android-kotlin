    package com.inshop.ui_model

    data class OrderSupplier (
        val id : String,
        val initialDate : String,
        val arrivalDate : String,
        val address: String,
        val total: Int,
        val shopId: String,
        val supplierId: String,
        ) {
            companion object {
                val EMPTY = OrderSupplier(
                     "",
                     "",
                     "",
                     "",
                     -1,
                     "",
                     "",
                )
            }
        }