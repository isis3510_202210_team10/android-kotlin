package com.inshop.ui_model


data class ShopProduct(
    val _id: String, val name:String, val description:String,
    val type:String, val image:String, val expirationDate:String,
    val priceSupplier: Int,
    val stock: Int,
    val shopId:String
)
