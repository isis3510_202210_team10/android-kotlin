package com.inshop.ui_model

data class SupplierShop(
    val _id : String,
    val name : String,
    val email : String,
    val address: String,
    val phone: String,
    val companyName: String,
)
