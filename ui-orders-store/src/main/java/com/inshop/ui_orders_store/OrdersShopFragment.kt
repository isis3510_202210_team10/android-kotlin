package com.inshop.ui_orders_store

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inshop.network_connection.NetworkConnection
import com.inshop.ui_model.OrderShop
import com.inshop.ui_orders_store.databinding.FragmentOrdersStoreBinding
import com.inshop.ui_orders_store.orders.OrdersShopAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class OrdersShopFragment : Fragment() {
    private var _binding: FragmentOrdersStoreBinding? = null
    private lateinit var adapter: OrdersShopAdapter
    private val ordersShop = mutableListOf<OrderShop>()
    private val ordersShopViewModel: OrdersShopViewModel by viewModels()

    private val binding get() = _binding!!

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOrdersStoreBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initRecyclerview()

        ordersShopViewModel.ordersShopModel.observe(viewLifecycleOwner, Observer {
            ordersShop.clear()
            adapter.notifyDataSetChanged()
            if(it.isNotEmpty()) {
                binding.fetchStatus.isVisible = false
                it.map { order ->
                    ordersShop.add(order) }

                binding.viewLoading.isVisible = false
                binding.viewOrders.isVisible = true

            } else {
                binding.viewLoading.isVisible = false
                binding.fetchStatus.isVisible = true
            }
        })

        NetworkConnection.getInstance(container!!.context).observe(viewLifecycleOwner) { isConnected ->
            binding.networkMessage.isVisible = !isConnected
            if(isConnected) {
                getOrders(container)
            }
        }

        val spinner = binding.ordersSpinner
        val list = listOf("ALL","ON_REVISION", "ACCEPTED", "DECLINED", "SHIPPING", "COMPLETED")

        if (container != null) {
            val spinnerAdapter = ArrayAdapter(container.context,android.R.layout.simple_spinner_item,list)
            spinner.adapter = spinnerAdapter
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    binding.viewLoading.isVisible = true
                    binding.viewOrders.isVisible = false
                    binding.fetchStatus.isVisible = false
                    if(p2>0){
                        val item = spinner.selectedItem.toString()
                        getOrdersBy(item, container)
                    }
                    else {
                        getOrders(container)
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    getOrders(container)
                }
            }
        }

        binding.ordersButtonAddOrder.setOnClickListener{
            showMessage(container)
        }

        return root
    }

    private fun getOrdersBy(item: String, container: ViewGroup) {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        ordersShopViewModel.getOrdersBy(idShop, item, isConnected)
    }

    private fun initRecyclerview() {
        adapter = OrdersShopAdapter(ordersShop)
        val recyclerView = binding.orderShopRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = adapter
    }

    private fun getOrders(container: ViewGroup) {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        ordersShopViewModel.getOrders(idShop, isConnected)
    }

    private fun showMessage(container: ViewGroup) {
        Toast.makeText(container.context, "In development", Toast.LENGTH_SHORT).show()
    }

    /**
     * Alert to notify the user from a message.
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { _, _ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}

