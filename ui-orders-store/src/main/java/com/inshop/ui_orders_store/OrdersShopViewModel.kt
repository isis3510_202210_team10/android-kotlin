package com.inshop.ui_orders_store

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.inshop.ui_model.OrderShop
import com.inshop.cases.OrdersShopCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrdersShopViewModel @Inject constructor(
    private val getOrdersShop: OrdersShopCase,
) : ViewModel() {

    val ordersShopModel = MutableLiveData<List<OrderShop>>()

    fun getOrders(idShop: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {

            val ordersList = mutableListOf<OrderShop>()
            val response = getOrdersShop.invoke(idShop, isConnected)

            ordersList.addAll(response)
            ordersShopModel.postValue(ordersList)

        }
    }

    fun getOrdersBy(idShop: String, item: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            val ordersList = mutableListOf<OrderShop>()
            val response = getOrdersShop.invoke(idShop, isConnected)

                response.map {
                    if (it.status == item)
                        ordersList.add(it)
                }

            ordersShopModel.postValue(ordersList)
        }
    }

    fun deleted() {
        viewModelScope.launch {
            getOrdersShop.delete()
        }
    }
}