package com.inshop.ui_orders_store.orders

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.OrderShop
import com.inshop.ui_orders_store.R


class OrdersShopAdapter(private val ordersList: List<OrderShop>): RecyclerView.Adapter<OrdersShopView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersShopView {
        val layoutInflater = LayoutInflater.from(parent.context)
        return OrdersShopView(layoutInflater.inflate(R.layout.item_ordershop, parent, false))
    }

    override fun onBindViewHolder(holder: OrdersShopView, position: Int) {
        val item = ordersList[position]
        val context: Context = holder.itemView.context
        holder.render(item, context)
    }

    override fun getItemCount(): Int = ordersList.size
}