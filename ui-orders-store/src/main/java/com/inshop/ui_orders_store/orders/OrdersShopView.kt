package com.inshop.ui_orders_store.orders

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.OrderShop
import com.inshop.ui_orders_store.R
import com.inshop.map.MapActivity

class OrdersShopView(view: View): RecyclerView.ViewHolder(view) {

    val date = view.findViewById<TextView>(R.id.order_item_Date)
    val PO = view.findViewById<TextView>(R.id.order_item_PO)
    val supplier = view.findViewById<TextView>(R.id.order_item_Supplier)
    val delivery = view.findViewById<TextView>(R.id.order_item_Delivery_Date)
    val total = view.findViewById<TextView>(R.id.order_item_Total)
    val map = view.findViewById<TextView>(R.id.order_Button_Map)
    val time = view.findViewById<Button>(R.id.order_Button_Time)
    val delete = view.findViewById<Button>(R.id.order_Button_Garbage)
    fun render(orderShop : OrderShop, context: Context) {
        date.text = orderShop.initialDate
        PO.text = orderShop.id
        supplier.text = orderShop.supplierId
        delivery.text = orderShop.arrivalDate
        total.text = orderShop.total.toString()
        map.setOnClickListener{
            val sharedPreference = context.getSharedPreferences("PREFERENCE_INSHOP_ORDER", Context.MODE_PRIVATE)
            val editor = sharedPreference?.edit()
            editor?.putString("DESTINATION_ADDRESS",orderShop.address)
            editor?.commit()

            val mapIntent = Intent(context, MapActivity::class.java)
            context.startActivity(mapIntent)
        }
        time.setOnClickListener {
            Toast.makeText(context, "In development", Toast.LENGTH_SHORT).show()
        }
        delete.setOnClickListener {
            Toast.makeText(context, "In development", Toast.LENGTH_SHORT).show()
        }
    }
}