package com.inshop.ui_orders_suppliers

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.inshop.ui_orders_suppliers.R
import com.inshop.ui_orders_suppliers.databinding.FragmentOrdersSupplierBinding
import com.inshop.ui_orders_suppliers.OrdersSupplierViewModel

class OrdersSupplierFragment : Fragment() {

    private var _binding: FragmentOrdersSupplierBinding? = null

    var navc: NavController?= null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val notificationsViewModel =
            ViewModelProvider(this).get(OrdersSupplierViewModel::class.java)

        _binding = FragmentOrdersSupplierBinding.inflate(inflater, container, false)
        val root: View = binding.root

/*        val textView: TextView = binding.textHome
        notificationsViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }*/
        return root
    }

}