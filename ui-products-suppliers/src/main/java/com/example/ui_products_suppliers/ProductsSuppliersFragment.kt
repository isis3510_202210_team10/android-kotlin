package com.example.ui_products_suppliers

import com.inshop.ui_model.ProductSupplier
import com.example.ui_products_suppliers.databinding.FragmentProductsSuppliersBinding
import com.example.ui_products_suppliers.products_suppliers.ProductsSuppliersAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.isVisible
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inshop.network_connection.NetworkConnection
import com.inshop.ui_model.ProductSupplierShop
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class ProductsSuppliersFragment : Fragment() {

    private var _binding: FragmentProductsSuppliersBinding? = null
    private lateinit var adapter: ProductsSuppliersAdapter
    private val suppliersShop = mutableListOf<ProductSupplierShop>()
    private val suppliersShopViewModel: ProductsSuppliersViewModel by viewModels()

    private val binding get() = _binding!!

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductsSuppliersBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initRecyclerview()

        suppliersShopViewModel.supplierShop.observe(viewLifecycleOwner, Observer {
            suppliersShop.clear()
            adapter.notifyDataSetChanged()
            if(it.isNotEmpty()) {
                binding.fetchStatus.isVisible = false
                it.map { supplier ->
                    suppliersShop.add(supplier) }

                binding.viewLoading.isVisible = false
                binding.viewOrders.isVisible = true

            } else {
                binding.viewLoading.isVisible = false
                binding.fetchStatus.isVisible = true
            }
        })

        NetworkConnection.getInstance(container!!.context).observe(viewLifecycleOwner) { isConnected ->
            binding.networkMessage.isVisible = !isConnected
            if(isConnected) {
               getSuppliers(container)
            }
        }

        getSuppliers(container)

        val homeViewModel =
            ViewModelProvider(this)[ProductsSuppliersViewModel::class.java]

        val textView: TextView = binding.ordersTextTimeUpdate
        val simpleDateFormat = SimpleDateFormat("yyyy.MM.dd HH:mm")
        val currentDateAndTime: String = simpleDateFormat.format(Date())
        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = "Last Update: " + currentDateAndTime.toString()
        }
        return root
    }


    private fun initRecyclerview() {
        adapter = ProductsSuppliersAdapter(suppliersShop)
        val recyclerView = binding.orderShopRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = adapter
    }

    private fun getSuppliers(container: ViewGroup) {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_INSHOP_SUPPLIER", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID_SUPPLIER", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        suppliersShopViewModel.getProductSuppliers(idShop, isConnected)
    }

    private fun showError() {

    }

    /**
     * Alert to notify the user from a message.
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { _, _ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}