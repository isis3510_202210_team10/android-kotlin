package com.example.ui_products_suppliers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.inshop.cases.ProductsSuppliersShopCase
import com.inshop.ui_model.OrderShop
import com.inshop.ui_model.ProductSupplier
import com.inshop.ui_model.ProductSupplierShop
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductsSuppliersViewModel @Inject constructor(
    private val getProductsSuppliersShop: ProductsSuppliersShopCase,
) : ViewModel() {
    val supplierShop = MutableLiveData<List<ProductSupplierShop>>()

    fun getProductSuppliers(idShop: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {

            val productsList = mutableListOf<ProductSupplierShop>()
            val response = getProductsSuppliersShop.invoke(idShop, isConnected)

            productsList.addAll(response)
            supplierShop.postValue(productsList)

        }
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text


}