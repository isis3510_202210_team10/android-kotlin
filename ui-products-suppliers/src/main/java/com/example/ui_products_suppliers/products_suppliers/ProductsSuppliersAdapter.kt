package com.example.ui_products_suppliers.products_suppliers

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.ProductSupplier
import com.example.ui_products_suppliers.R
import com.inshop.ui_model.ProductSupplierShop


class ProductsSuppliersAdapter(private val ordersList: List<ProductSupplierShop>): RecyclerView.Adapter<ProductsSuppliersView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsSuppliersView {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ProductsSuppliersView(layoutInflater.inflate(R.layout.item_product_supplier, parent, false))
    }

    override fun onBindViewHolder(holder: ProductsSuppliersView, position: Int) {
        val item = ordersList[position]
        val context: Context = holder.itemView.context
        holder.render(item, context)
    }

    override fun getItemCount(): Int = ordersList.size
}