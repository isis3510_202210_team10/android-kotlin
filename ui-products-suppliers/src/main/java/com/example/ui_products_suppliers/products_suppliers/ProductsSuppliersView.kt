package com.example.ui_products_suppliers.products_suppliers

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ui_products_suppliers.R
import com.inshop.ui_model.ProductSupplier
import com.inshop.ui_model.ProductSupplierShop

class ProductsSuppliersView(view: View): RecyclerView.ViewHolder(view) {

    val id_product = view.findViewById<TextView>(R.id.order_item_Date)
    val name = view.findViewById<TextView>(R.id.order_item_Supplier)
    val description = view.findViewById<TextView>(R.id.order_item_Delivery_Date)
    val unit_price = view.findViewById<TextView>(R.id.order_item_Total)
    val stock = view.findViewById<TextView>(R.id.order_item_Stock)

    fun render(prodcutSupplierShop : ProductSupplierShop, context: Context) {
        id_product.text = prodcutSupplierShop._id
        name.text = prodcutSupplierShop.name
        description.text = prodcutSupplierShop.description
        unit_price.text = prodcutSupplierShop.priceSupplier.toString() + " COP."
        stock.text= prodcutSupplierShop.stock.toString() + " Units."
    }
}