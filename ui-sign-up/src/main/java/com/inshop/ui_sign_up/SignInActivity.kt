package com.inshop.ui_sign_up

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import com.example.data_remote.inshop.model.ShopResponse
import com.example.data_remote.inshop.model.SupplierResponse
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.inshop.network_connection.NetworkConnection
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.inshop.ui_lateral_menu.ActivityBottomNavigator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This class manage the sign in functionality.
 * @author Sergio Julian Zona Moreno
 */
@Suppress("SpellCheckingInspection")
class SignInActivity : AppCompatActivity() {
    /**
     * Authentication attribute from firebase.
     */
    private lateinit var auth: FirebaseAuth

    /**
     * Constructor of the activity
     */
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Hide the top bar in this activity before it renders
        supportActionBar?.hide()

        // Eventual connectivity
        NetworkConnection.getInstance(applicationContext).observe(this) { isConnected ->
            if (isConnected){
                setContentView(R.layout.activity_sign_in)

                // Verify if the user sign up correctly and shows the notification message
                verifySignUp()

                // Hide keyboard when clicking background
                hideKeyboard()

                auth = Firebase.auth
                interactions()
                signIn()
                val emailTxt = findViewById<TextInputEditText>(R.id.email_input_txt)
                emailFocusListener(emailTxt)
            }
            else{
                setContentView(R.layout.no_connection)
                val tryAgain = findViewById<TextView>(R.id.btn_please_try_again)
                tryAgain.setOnClickListener {
                    showAlert("Notification","There is no internet connection...")
                }
            }
        }
    }

    /**
     * This method takes all buttons and posible interactions and set their corresponding event listeners.
     */
    private fun interactions(){
        // Button sign up
        val signUp = findViewById<TextView>(R.id.create_account_txt)
        signUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        // Text forgot password
        val forgot = findViewById<TextView>(R.id.forgot_password_txt)
        forgot.setOnClickListener {
            showAlert("Change of password","To change your user's password please contact: \n -> InShop@gmail.com")
        }

        // Button Google
        val google = findViewById<TextView>(R.id.btn_google)
        google.setOnClickListener {
            showAlert("In development","Authentication with Google will be available soon.")
        }

        // Button Microsoft
        val microsoft = findViewById<TextView>(R.id.btn_microsoft)
        microsoft.setOnClickListener {
            showAlert("In development","Authentication with Microsoft will be available soon.")
        }
    }

    /**
     * Function that sign in a user. It validates in Firebase, and also retrieve his/her information from the API Rest.
     */
    private fun signIn(){
        val signIn = findViewById<Button>(R.id.btn_sign_in)
        val emailTxt = findViewById<TextInputEditText>(R.id.email_input_txt)
        emailTxt.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(50))
        val passwordTxt = findViewById<TextInputEditText>(R.id.password_input_txt)
        passwordTxt.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(25))

        signIn.setOnClickListener {
            if(emailTxt.error == null && emailTxt.text!!.isNotEmpty() && passwordTxt.text!!.isNotEmpty())
            {
                val originalWidth = signIn.layoutParams.width
                animateButton(signIn)
                val layoutParams: ConstraintLayout.LayoutParams =
                    signIn.layoutParams as ConstraintLayout.LayoutParams

                auth.signInWithEmailAndPassword(emailTxt.text.toString(),
                    passwordTxt.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        RetrofitClient.INSTANCE.getShopByEmail(emailTxt.text.toString())
                            .enqueue(object: Callback<ShopResponse> {
                                override fun onResponse(call: Call<ShopResponse>, response: Response<ShopResponse>) {
                                    // Save user information in SharedPreferences
                                    val sharedPreference =  getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)
                                    val editor = sharedPreference.edit()
                                    editor.putString("ID",response.body()?._id.toString())
                                    editor.putString("Name",response.body()?.name.toString())
                                    editor.putString("Email",response.body()?.email.toString())
                                    editor.putString("UserType", "Shop")
                                    editor.apply()

                                    showHome()
                                }
                                override fun onFailure(call: Call<ShopResponse>, t: Throwable) {
                                    RetrofitClient.INSTANCE.getSupplierByEmail(emailTxt.text.toString())
                                        .enqueue(object: Callback<SupplierResponse> {
                                            override fun onResponse(call: Call<SupplierResponse>, response: Response<SupplierResponse>) {
                                                // Save user information in SharedPreferences
                                                val sharedPreference =  getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)
                                                val editor = sharedPreference.edit()
                                                editor.putString("ID",response.body()?._id.toString())
                                                editor.putString("Name",response.body()?.name.toString())
                                                editor.putString("Email",response.body()?.email.toString())
                                                editor.putString("UserType", "Supplier")
                                                editor.apply()

                                                showHome()
                                            }
                                            override fun onFailure(call: Call<SupplierResponse>, t: Throwable) {
                                                // Stop button animation
                                                signIn.isEnabled = true
                                                signIn.isClickable = true
                                                signIn.hideProgress("Sign in")
                                                layoutParams.width = originalWidth
                                                signIn.layoutParams = layoutParams
                                                showAlert("Sign in error","The credentials are not correct. Please check and try again!")
                                            }
                                        })
                                }
                            })
                    } else {
                        // Stop button animation
                        signIn.hideProgress("Sign in")
                        signIn.isEnabled = true
                        signIn.isClickable = true
                        layoutParams.width = originalWidth
                        signIn.layoutParams = layoutParams
                        showAlert("Sign in error","The credentials are not correct. Please check and try again!")
                    }
                }
            }
            else if(emailTxt.error != null){
                showAlert("Invalid fields", "Check the email field. Please.")
            }
            else if(emailTxt.error == null ){
                showAlert("Invalid fields", "The fields can not be empty")
            }
        }
    }

    /**
     * Listener to the Email
     */
    private fun emailFocusListener(emailTxt: TextInputEditText)
    {
        emailTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                emailTxt.error = validateEmail(emailTxt)
            }
        }

        emailTxt.doOnTextChanged { _, _, _, _ ->
            emailTxt.error = validateEmail(emailTxt)
        }
    }

    /**
     * Validate the Email
     */
    private fun validateEmail(emailTxt: TextInputEditText): String?
    {
        if(emailTxt.text?.length == 0){
            return "This field is required"
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(emailTxt.text.toString()).matches())
        {
            return "Invalid email address"
        }
        return null
    }

    /**
     * Alert to notify the user from a message.
     */
    private fun showAlert(title: String,message: String){
        val builder = AlertDialog.Builder(this, R.style.AlertDialogBlackTheme)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok"){ _,_ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    /**
     *  This method animate the button as a Progress Button.
     */
    private fun animateButton(button: Button) {
        button.isEnabled = false
        button.isClickable = false

        val layoutParams: ConstraintLayout.LayoutParams =
            button.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.width = 500
        button.layoutParams = layoutParams

        // bind your button to activity lifecycle
        bindProgressButton(button)

        // (Optional) Enable fade In / Fade out animations
        button.attachTextChangeAnimator()

        // Show progress with "Loading" text
        button.showProgress {
            buttonTextRes = R.string.please_wait
            progressColor = Color.BLACK
        }
    }

    /**
     * Muestra la vista home.
     */
    private fun showHome(){
        val homeIntent = Intent(this, ActivityBottomNavigator::class.java)
        this.finish() // Finish the current sign in activity so the user can't go back and make weird things.
        startActivity(homeIntent)
    }

    /**
     * Hide keyboard when clicking the background
     */
    private fun hideKeyboard(){
        val layout = findViewById<ConstraintLayout>(R.id.activity_sign_in_layout)
        layout.setOnClickListener {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    /**
     * Verify if the user sign up.
     */
    private fun verifySignUp(){
        val sharedPreference =  getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val signUpNotification = sharedPreference.getString("COME_FROM_SIGN_UP", "False")!!

        if(signUpNotification!="False"){
            showAlert("Notificacion", "The user was created successfully, please try to login with your credentials.")
            val editor = sharedPreference.edit()
            editor.putString("COME_FROM_SIGN_UP", "False")
            editor.apply()
        }
    }
}