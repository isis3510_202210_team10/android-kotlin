package com.inshop.ui_sign_up

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.example.data_remote.inshop.model.CreateResponse
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.inshop.network_connection.NetworkConnection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This class manage the sign up functionality.
 * @author Sergio Julian Zona Moreno
 */
@Suppress("SpellCheckingInspection")
class SignUpActivity : AppCompatActivity() {
    /**
     * Authentication attribute from firebase.
     */
    private lateinit var auth: FirebaseAuth

    private var originalWidth : Int = 0

    /**
     * Constructor of the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Hide the top bar in this activity before it renders
        supportActionBar?.hide()

        // Initialize the layout
        setContentView(R.layout.activity_sign_up)
        dropdownMenuCreation()

        // Hide keyboard when clicking background
        hideKeyboard()

        // Get all fields
        val signUp = findViewById<Button>(R.id.btn_sign_up)
        val companyNameTxt = findViewById<TextInputEditText>(R.id.company_name_input_sign_up_txt)
        val emailTxt = findViewById<TextInputEditText>(R.id.email_input_sign_up_txt)
        val addressTxt = findViewById<TextInputEditText>(R.id.address_input_sign_up_txt)
        val phoneTxt = findViewById<TextInputEditText>(R.id.phone_input_sign_up_txt)
        val userTypeTxt = findViewById<AutoCompleteTextView>(R.id.type_user_filled_exposed_dropdown)
        val passwordTxt = findViewById<TextInputEditText>(R.id.password_input_sign_up_txt)
        val repeatPasswordTxt = findViewById<TextInputEditText>(R.id.repeat_password_input_sign_up_txt)

        limitFields(companyNameTxt, emailTxt, addressTxt, phoneTxt, userTypeTxt, passwordTxt, repeatPasswordTxt)

        originalWidth = signUp.layoutParams.width

        val signIn = findViewById<TextView>(R.id.sign_in_underlined_txt)
        signIn.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }

        auth = Firebase.auth
        userCreation(signUp, companyNameTxt, emailTxt, addressTxt, phoneTxt, userTypeTxt, passwordTxt, repeatPasswordTxt)
        verifyFields(companyNameTxt, emailTxt, addressTxt, phoneTxt, userTypeTxt, passwordTxt, repeatPasswordTxt)
        eventualConnectivity()
    }

    /**
     * This method creates de dropdown menu in the form.
     */
    private fun dropdownMenuCreation(){
        val userTypes = findViewById<AutoCompleteTextView>(R.id.type_user_filled_exposed_dropdown)
        val adapter= ArrayAdapter.createFromResource(this, R.array.user_types, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        userTypes.setAdapter(adapter)
    }

    /**
     * Limit all the fields to maximum quantity of characters.
     */
    private fun limitFields(companyNameTxt: TextInputEditText, emailTxt: TextInputEditText,
                            addressTxt: TextInputEditText, phoneTxt: TextInputEditText, userTypeTxt: AutoCompleteTextView,
                            passwordTxt: TextInputEditText, repeatPasswordTxt: TextInputEditText)
    {
        companyNameTxt.filters = arrayOf<InputFilter>(LengthFilter(50))
        emailTxt.filters = arrayOf<InputFilter>(LengthFilter(100))
        addressTxt.filters = arrayOf<InputFilter>(LengthFilter(100))
        phoneTxt.filters = arrayOf<InputFilter>(LengthFilter(15))
        userTypeTxt.filters = arrayOf<InputFilter>(LengthFilter(10))
        passwordTxt.filters = arrayOf<InputFilter>(LengthFilter(25))
        repeatPasswordTxt.filters = arrayOf<InputFilter>(LengthFilter(25))
    }

    /**
     * Function that creates a user.
     */
    private fun userCreation(
        signUp: Button,
        companyNameTxt: TextInputEditText,
        emailTxt: TextInputEditText,
        addressTxt: TextInputEditText,
        phoneTxt: TextInputEditText,
        userTypeTxt: AutoCompleteTextView,
        passwordTxt: TextInputEditText,
        repeatPasswordTxt: TextInputEditText
    ) {
        signUp.setOnClickListener {

            // Firebase authentication
            if (submitForm(
                    companyNameTxt, emailTxt,
                    addressTxt,
                    phoneTxt,
                    userTypeTxt,
                    passwordTxt,
                    repeatPasswordTxt
                )
                && fieldsNotEmpty(
                    companyNameTxt,
                    emailTxt,
                    addressTxt,
                    phoneTxt,
                    userTypeTxt,
                    passwordTxt,
                    repeatPasswordTxt
                )
            ) {
                // Animate button
                animateButton(signUp)

                createAccount(
                    companyNameTxt.text.toString(),
                    emailTxt.text.toString(),
                    addressTxt.text.toString(),
                    phoneTxt.text.toString(),
                    userTypeTxt.text.toString(),
                    passwordTxt.text.toString()
                )
            }
        }
    }

    /**
     * Add all focus listeners in one function that verify each field.
     */
    private fun verifyFields(companyNameTxt: TextInputEditText, emailTxt: TextInputEditText, addressTxt: TextInputEditText,
                             phoneTxt: TextInputEditText, userTypeTxt: AutoCompleteTextView, passwordTxt: TextInputEditText, repeatPasswordTxt: TextInputEditText){
        nameFocusListener(companyNameTxt)
        emailFocusListener(emailTxt)
        addressFocusListener(addressTxt)
        phoneFocusListener(phoneTxt)
        userTypeFocusListener(userTypeTxt)
        passwordFocusListener(passwordTxt)
        repeatPasswordFocusListener(repeatPasswordTxt, passwordTxt)
    }

    /**
     * Submit the form, or shows the user what are his/her mistakes
     */
    private fun submitForm(companyNameTxt: TextInputEditText, emailTxt: TextInputEditText, addressTxt: TextInputEditText,
                           phoneTxt: TextInputEditText, userTypeTxt: AutoCompleteTextView, passwordTxt: TextInputEditText, repeatPasswordTxt: TextInputEditText): Boolean {
        // Last verification for the fields.
        companyNameTxt.error = validateName(companyNameTxt)
        emailTxt.error = validateEmail(emailTxt)
        addressTxt.error = validateAddress(addressTxt)
        phoneTxt.error = validatePhone(phoneTxt)
        userTypeTxt.error = validateUserType(userTypeTxt)
        passwordTxt.error = validatePassword(passwordTxt)
        repeatPasswordTxt.error = validateRepeatPassword(repeatPasswordTxt, passwordTxt)

        val companyNameError = companyNameTxt.error == null
        val emailError = emailTxt.error == null
        val addressError = addressTxt.error == null
        val phoneError = phoneTxt.error == null
        val userTypeError = userTypeTxt.error == null
        val passwordError = passwordTxt.error == null
        val repeatPasswordError = repeatPasswordTxt.error == null

        return if(companyNameError && emailError && addressError && phoneError && userTypeError && passwordError && repeatPasswordError ) {
            true
        } else{
            validateForm(companyNameTxt, emailTxt, addressTxt, phoneTxt, userTypeTxt, passwordTxt, repeatPasswordTxt)
            false
        }
    }

    /**
     * This method validates the attributes in the form and return all the failures presented
     */
    private fun validateForm(companyNameTxt: TextInputEditText, emailTxt: TextInputEditText, addressTxt: TextInputEditText,
                             phoneTxt: TextInputEditText, userTypeTxt: AutoCompleteTextView, passwordTxt: TextInputEditText, repeatPasswordTxt: TextInputEditText)
    {
        var message = ""
        if(companyNameTxt.error != null)
            message += "\n\nCompany name: " + companyNameTxt.error
        if(emailTxt.error != null)
            message += "\n\nEmail: " + emailTxt.error
        if(addressTxt.error != null)
            message += "\n\nAddress: " + addressTxt.error
        if(phoneTxt.error != null)
            message += "\n\nPhone: " + phoneTxt.error
        if(userTypeTxt.error != null)
            message += "\n\nUser type: " + userTypeTxt.error
        if(passwordTxt.error != null)
            message += "\n\nPassword: " + passwordTxt.error
        if(repeatPasswordTxt.error != null)
            message += "\n\nRepeat password: " + repeatPasswordTxt.error

        showAlert("Invalid form",message)
    }

    /**
     * Listener to the Name
     */
    private fun nameFocusListener(companyNameTxt: TextInputEditText)
    {
        companyNameTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                companyNameTxt.error = validateName(companyNameTxt)
            }
        }

        companyNameTxt.doOnTextChanged { _, _, _, _ ->
            companyNameTxt.error = validateName(companyNameTxt)
        }
    }

    /**
     * Validate the Name
     */
    private fun validateName(companyNameTxt: TextInputEditText): String?
    {
        if(companyNameTxt.text?.length == 0){
            return "This field is required"
        }

        if(companyNameTxt.text?.length!! < 3){
            return "The name required at least 3 characters long"
        }
        return null
    }

    /**
     * Listener to the Email
     */
    private fun emailFocusListener(emailTxt: TextInputEditText)
    {
        emailTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                emailTxt.error = validateEmail(emailTxt)
            }
        }

        emailTxt.doOnTextChanged { _, _, _, _ ->
            emailTxt.error = validateEmail(emailTxt)
        }
    }

    /**
     * Validate the Email
     */
    private fun validateEmail(emailTxt: TextInputEditText): String?
    {
        if(emailTxt.text?.length == 0){
            return "This field is required"
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(emailTxt.text.toString()).matches())
        {
            return "Invalid email address"
        }
        return null
    }

    /**
     * Listener to the Address
     */
    private fun addressFocusListener(addressTxt: TextInputEditText)
    {
        addressTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                addressTxt.error = validateAddress(addressTxt)
            }
        }

        addressTxt.doOnTextChanged { _, _, _, _ ->
            addressTxt.error = validateAddress(addressTxt)
        }
    }

    /**
     * Validate the Address
     */
    private fun validateAddress(addressTxt: TextInputEditText): String?
    {
        if(addressTxt.text?.length == 0){
            return "This field is required"
        }

        if(addressTxt.text?.length!! < 6){
            return "The address required at least 6 characters long"
        }
        return null
    }

    /**
     * Listener to the Phone
     */
    private fun phoneFocusListener(phoneTxt: TextInputEditText)
    {
        phoneTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                phoneTxt.error = validatePhone(phoneTxt)
            }
        }

        phoneTxt.doOnTextChanged { _, _, _, _ ->
            phoneTxt.error = validatePhone(phoneTxt)
        }
    }

    /**
     * Validate the Phone
     */
    private fun validatePhone(phoneTxt: TextInputEditText): String?
    {
        if(phoneTxt.text?.length == 0){
            return "This field is required"
        }

        if(!phoneTxt.text?.matches(".*[0-9].*".toRegex())!!)
        {
            return "Must be all Digits"
        }

        if(phoneTxt.text?.length!! < 7){
            return "The phone required at least 7 digits long"
        }

        return null
    }

    /**
     * Listener to the UserType
     */
    private fun userTypeFocusListener(userTypeTxt: AutoCompleteTextView)
    {
        userTypeTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                userTypeTxt.error = validateUserType(userTypeTxt)
            }
        }

        userTypeTxt.doOnTextChanged { _, _, _, _ ->
            userTypeTxt.error = validateUserType(userTypeTxt)
        }
    }

    /**
     * Validate the UserType
     */
    private fun validateUserType(userTypeTxt: AutoCompleteTextView): String?
    {
        if(userTypeTxt.text?.length == 0){
            return "This field is required"
        }

        if(userTypeTxt.text.toString() != "Shop" && userTypeTxt.text.toString() != "Supplier"){
            return "You must select Shop or Supplier. There are not other roles available."
        }
        return null
    }

    /**
     * Listener to the Password
     */
    private fun passwordFocusListener(passwordTxt: TextInputEditText)
    {
        passwordTxt.setOnFocusChangeListener { _, focused ->
            if(focused)
            {
                passwordTxt.error = validatePassword(passwordTxt)
            }
        }

        passwordTxt.doOnTextChanged { _, _, _, _ ->
            passwordTxt.error = validatePassword(passwordTxt)
        }
    }

    /**
     * Validate the Password
     */
    private fun validatePassword(passwordTxt: TextInputEditText): String?
    {
        var req = ""
        if(passwordTxt.text?.length == 0){
            return "This field is required"
        }
        if(passwordTxt.text?.length!! < 8)
        {
            req += "The password required at least 8 characters long\n"
        }
        if(!passwordTxt.text?.matches(".*[A-Z].*".toRegex())!!)
        {
            req += "The password must contain at least 1 upper-case character\n"
        }
        if(!passwordTxt.text?.matches(".*[a-z].*".toRegex())!!)
        {
            req += "The password must contain at least 1 lower-case character\n"
        }
        if(!passwordTxt.text?.matches(".*[0-9].*".toRegex())!!)
        {
            req += "The password must contain at least 1 number\n"
        }
        if(!passwordTxt.text?.matches(".*[@#$%^&+=.!].*".toRegex())!!)
        {
            req += "The password must contain at least 1 special character (@#$%^&+=.!)\n"
        }
        if (req != ""){
            return req
        }
        return null
    }

    /**
     * Listener to the Password
     */
    private fun repeatPasswordFocusListener(repeatPasswordTxt: TextInputEditText, passwordTxt: TextInputEditText)
    {
        repeatPasswordTxt.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                repeatPasswordTxt.error = validateRepeatPassword(repeatPasswordTxt, passwordTxt)
            }
        }

        repeatPasswordTxt.doOnTextChanged { _, _, _, _ ->
            repeatPasswordTxt.error = validateRepeatPassword(repeatPasswordTxt, passwordTxt)
        }
    }

    /**
     * Validate the Password
     */
    private fun validateRepeatPassword(repeatPasswordTxt: TextInputEditText, passwordTxt: TextInputEditText): String?
    {
        if(passwordTxt.text?.length == 0){
            return "This field is required"
        }
        if(passwordTxt.text.toString() != repeatPasswordTxt.text.toString())
        {
            return "Both passwords must match"
        }
        return null
    }

    /**
     * This function create the account with the Email and Password taken before.
     */
    private fun createAccount(companyName: String, email: String, address: String,
                              phone:String, type: String, password:String)
    {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener{ task ->
                if (task.isSuccessful) {
                    when (type) {
                        "Shop" -> {
                            RetrofitClient.INSTANCE.createShop(companyName, email, address, phone, "FOOD")
                                .enqueue(object:Callback<CreateResponse>{
                                    override fun onResponse(call: Call<CreateResponse>, response: Response<CreateResponse>) {
                                        showSignIn(task.result.user?.email, type)
                                    }

                                    override fun onFailure(call: Call<CreateResponse>, t: Throwable) {
                                        // Stop button animation
                                        stopAnimation()

                                        showAlert("Error","There was an error processing the operation. Try again in a few minutes.")
                                    }
                                })
                        }
                        "Supplier" -> {
                            RetrofitClient.INSTANCE.createSupplier(companyName, email, address, phone, companyName)
                                .enqueue(object:Callback<CreateResponse>{
                                    override fun onResponse(call: Call<CreateResponse>, response: Response<CreateResponse>) {
                                        showSignIn(task.result.user?.email, type)
                                    }

                                    override fun onFailure(call: Call<CreateResponse>, t: Throwable) {
                                        // Stop button animation
                                        stopAnimation()

                                        showAlert("Error","There was an error processing the operation. Try again in a few minutes.")
                                    }
                                })
                        }
                        else -> {
                            // Stop button animation
                            stopAnimation()

                            showAlert("Error","There was an error processing the operation. Try again in a few minutes.")
                        }
                    }
                } else {
                    // Stop button animation
                    stopAnimation()

                    // User exists
                    showAlert("Error","This email has already an account. Try to login in with your credentials.")
                }
            }
    }

    /**
     * Verifies that the fields on the form are not empty.
     */
    private fun fieldsNotEmpty(companyNameTxt: TextInputEditText, emailTxt:TextInputEditText, addressTxt: TextInputEditText,
                               phoneTxt: TextInputEditText, userTypeTxt:AutoCompleteTextView, passwordTxt: TextInputEditText,
                               repeatPasswordTxt:TextInputEditText): Boolean {
        return (companyNameTxt.text!!.isNotEmpty()) && (emailTxt.text!!.isNotEmpty()) && (addressTxt.text!!.isNotEmpty())
                && (phoneTxt.text!!.isNotEmpty()) && (userTypeTxt.text!!.isNotEmpty()) && (passwordTxt.text!!.isNotEmpty())
                && (repeatPasswordTxt.text!!.isNotEmpty())
    }

    /**
     * Alert to notify the user from a message.
     */
    @Suppress("DEPRECATION")
    private fun showAlert(title: String, message: String){
        val builder = AlertDialog.Builder(this, R.style.AlertDialogBlackTheme)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok"){ _,_ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    /**
     *  This method animate the button as a Progress Button.
     */
    private fun animateButton(button: Button) {
        CoroutineScope(Dispatchers.Main).launch {
            runOnUiThread {
                button.isEnabled = false
                button.isClickable = false

                val layoutParams: ConstraintLayout.LayoutParams =
                    button.layoutParams as ConstraintLayout.LayoutParams
                layoutParams.width = 500
                button.layoutParams = layoutParams

                // bind your button to activity lifecycle
                bindProgressButton(button)

                // (Optional) Enable fade In / Fade out animations
                button.attachTextChangeAnimator()

                // Show progress with "Loading" text
                button.showProgress {
                    buttonTextRes = R.string.please_wait
                    progressColor = Color.BLACK
                }
            }
        }
    }

    /**
     *  This method stop the animation of the Sign Up Progress Button
     */
    private fun stopAnimation() {
        val signUp = findViewById<Button>(R.id.btn_sign_up)
        val layoutParams: ConstraintLayout.LayoutParams =
            signUp.layoutParams as ConstraintLayout.LayoutParams
        signUp.hideProgress("Sign up")
        signUp.isEnabled = true
        signUp.isClickable = true
        layoutParams.width = originalWidth
        signUp.layoutParams = layoutParams
    }

    /**
     * Redirect to the home view.
     */
    @SuppressLint("ApplySharedPref")
    private fun showSignIn(email: String?, type: String){
        val signInIntent = Intent(this, SignInActivity::class.java).apply{
            putExtra("email",email)
            putExtra("type", type)
        }

        // Preferences that allows to show a message in the other view
        val sharedPreference =  getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val editor = sharedPreference.edit()
        editor.putString("COME_FROM_SIGN_UP", "True")
        editor.commit()

        this.finish() // Finish the current sign up activity so the user can't go back and make weird things.
        startActivity(signInIntent)
    }

    /**
     * Hide keyboard
     */
    private fun hideKeyboard(){
        val layout = findViewById<ConstraintLayout>(R.id.activity_sign_up_layout)
        layout.setOnClickListener {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    /**
     * Manage the eventual connectivity issue on the Sign Up view.
     */
    private fun eventualConnectivity(){
        val signUp = findViewById<Button>(R.id.btn_sign_up)
        val warning = findViewById<TextView>(R.id.warning_eventual_connectivity_txt)
        NetworkConnection.getInstance(applicationContext).observe(this) { isConnected ->
            if (isConnected) {
                // Enable the Sign Up button
                signUp.isEnabled = true
                signUp.setTextColor(ContextCompat.getColor(applicationContext, R.color.black))

                // Hide the bottom Warning
                warning.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.principal_carga))
                warning.text = ""
            }
            else{
                showAlert(
                    "Network connection problem",
                    "The registration functionality is not available since it is not connected to the WIFI/mobile network"
                )

                // Disable the Sign Up button
                signUp.isEnabled = false
                signUp.setTextColor(ContextCompat.getColor(applicationContext, R.color.warning_color))

                // Show the bottom warning
                warning.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.warning_color))
                warning.text = getString(R.string.there_is_no_internet_connection)
            }
        }
    }
}