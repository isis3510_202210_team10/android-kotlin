package com.inshop.ui_stats

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import io.github.reactivecircus.cache4k.Cache
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.time.Duration.Companion.minutes

/**
 * This Fragment manage the Stats functionality.
 * @author Sergio Julian Zona Moreno
 */
@Suppress("SpellCheckingInspection")
class StatsFragment : Fragment() {
    /**
     * Bar chart that shows the stock per product.
     */
    private lateinit var barChart: BarChart

    /**
     * Array of the stock price by product.
     */
    private var stockPrice = ArrayList<Product>()

    private lateinit var cache : Cache<String, ArrayList<Product>>

    /**
     * Function that initialize the fragment.
     */
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Create the layout that is going to be used
        val layout = inflater.inflate(R.layout.fragment_stats, container, false)

        // Find the current date.
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")
        val date = current.format(formatter)

        val lastUpdateTxt = layout.findViewById<TextView>(R.id.last_update_txt)
        lastUpdateTxt?.text = lastUpdateTxt?.text.toString() + " " + date.toString()

        cache = Cache.Builder().
                expireAfterAccess(30.minutes). // Time based eviction policy
                maximumCacheSize(10). // Size based eviction policy
                build<String, ArrayList<Product>>()

        // Setup the Chart
        barChart = layout.findViewById(R.id.bar_chart)
        barChart.setNoDataText("Data is charging...")
        barChart.setNoDataTextColor(ContextCompat.getColor(requireActivity(), R.color.principal_carga))
        initBarChart(barChart)
        getProducts(layout, barChart)

        return layout
    }

    private fun initBarChart(barChart: BarChart) {
        // Hide grid lines
        barChart.axisLeft.setDrawGridLines(true)
        val xAxis: XAxis = barChart.xAxis
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)

        // Remove right Y-axis
        barChart.axisRight.isEnabled = false

        // Remove legend
        barChart.legend.isEnabled = false

        // Remove description label
        barChart.description.isEnabled = false

        // Add animation when loading
        barChart.animateY(2000)

        // Draw labels on X-axis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.valueFormatter = MyAxisFormatter()
        xAxis.setDrawLabels(true)
        xAxis.granularity = 1f
        //xAxis.labelRotationAngle = -90f
    }

    inner class MyAxisFormatter : IndexAxisValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if(index < stockPrice.size) {
                 stockPrice[index].name
            } else {
                ""
            }
        }
    }

    data class Product(
        val id: String,
        val name:String,
        val stock: Int,
    )

    private fun drawChart(barChart: BarChart, products: ArrayList<Product>){
        val entries: ArrayList<BarEntry> = ArrayList()
        var count=0
        for (i in products) {
            entries.add(BarEntry(count.toFloat(), i.stock.toFloat()))
            count++
        }

        val barDataSet = BarDataSet(entries, "")
        barDataSet.setColors(ContextCompat.getColor(requireActivity(), R.color.principal_carga))

        val data = BarData(barDataSet)
        barChart.data = data

        barChart.invalidate()
        barChart.refreshDrawableState()
    }

    private fun getProducts(layout: View, barChart: BarChart)
    {
        val stockTxt = layout.findViewById<TextView>(R.id.stock_txt)
        val productTxt = layout.findViewById<TextView>(R.id.product_txt)
        val sharedPreference =  activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID", "")!!

        CoroutineScope(Dispatchers.IO).launch {
            stockPrice = cache.get("StockProducts") {
                fetchProductsById(idShop) // Call (might be a suspend function)
            }

            requireActivity().runOnUiThread{
                // Render de Chart
                drawChart(barChart, stockPrice)

                // Show the axis labels
                stockTxt.visibility = View.VISIBLE
                productTxt.visibility = View.VISIBLE
            }
        }
    }

    private suspend fun fetchProductsById(idShop:String): ArrayList<Product> {
        val instance = RetrofitClient.INSTANCE.getProductsByShopID(idShop)
        val response = instance.body()
        if (instance.isSuccessful) {
            for (product in response!!) {
                val productUI =
                    Product(product._id, product.name.trim().split(" ")[0], product.stock.toInt())
                stockPrice.add(productUI)
            }
            cache.put("StockProducts", stockPrice)
        }
        return stockPrice
    }

    /**
     * Alert to notify the user from a message.
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String){
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok"){ _,_ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}