package com.inshop.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.inshop.store.databinding.FragmentStoreBinding
import kotlinx.android.synthetic.main.fragment_store.*

class StoreFragment : Fragment() ,View.OnClickListener{

    private var _binding: FragmentStoreBinding? = null
    var navc: NavController ?= null
    var but: ImageButton ?= null
    var id:String? = null
    var name:String? = null
    var userType:String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val dashboardViewModel =
            ViewModelProvider(this).get(StoreViewModel::class.java)

        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //val textView: TextView = binding.
        //dashboardViewModel.text.observe(viewLifecycleOwner) {
        //    textView.text = it
       // }

      /*  val imageBtn_orders = root.findViewById<ImageButton>(R.id.imageBtn_orders)
        imageBtn_orders.setOnClickListener {
            val secondFragment = OrdersFragment()
            val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
            transaction.replace(R.id.drawer_layout,secondFragment)
            transaction.commit()
        }*/

        // Gets the data from the passed bundle
        val bundle = arguments
/*        Log.d("StoreFragment", bundle.toString())
        id = bundle!!.getString("id")
        id?.let { Log.d("StoreFragment", it) }
        name = bundle!!.getString("name")
        userType = bundle!!.getString("userType")*/



        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navc= Navigation.findNavController(view)
        view.findViewById<ImageButton>(R.id.imageBtn_orders)?.setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_orders)?.setOnClickListener(this)

        view.findViewById<ImageButton>(R.id.imageBtn_inventory)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_navigation_inventory)
        }
        view.findViewById<Button>(R.id.btn_inventory)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_navigation_inventory)
        }
        view.findViewById<Button>(R.id.btn_alerts)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_alerts_store)
        }

        view.findViewById<ImageButton>(R.id.imageBtn_alerts)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_alerts_store)
        }
        view.findViewById<Button>(R.id.btn_suppliers)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_suppliers)
        }
        view.findViewById<ImageButton>(R.id.imageBtn_suppliers)?.setOnClickListener {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_suppliers)
        }
    }

    override fun onClick(p0: View?) {
        if(userType=="Supplier")
        {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_navigation_orders_supplier)
        }
        else
        {
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_store_to_navigation_orders_shop3)
        }

    }
}