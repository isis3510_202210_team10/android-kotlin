package com.inshop.ui_suppliers

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.inshop.ui_model.SupplierShop
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.inshop.network_connection.NetworkConnection
import com.inshop.ui_suppliers.databinding.FragmentSuppliersBinding
import com.inshop.ui_suppliers.suppliers.SuppliersAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SuppliersFragment : Fragment() {
    private var _binding: FragmentSuppliersBinding? = null
    private lateinit var adapter: SuppliersAdapter
    private val suppliersShop = mutableListOf<SupplierShop>()
    private lateinit var navc: NavController
    private val suppliersShopViewModel: SuppliersViewModel by viewModels()
    private lateinit var containerGlob: ViewGroup
    private val binding get() = _binding!!

    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {
        _binding = FragmentSuppliersBinding.inflate(inflater, container, false)
        val root: View = binding.root
//        navc= Navigation.findNavController(root)
//        initRecyclerview()

        suppliersShopViewModel.supplierShop.observe(viewLifecycleOwner, Observer {
            suppliersShop.clear()
            adapter.notifyDataSetChanged()
            if(it.isNotEmpty()) {
                binding.fetchStatus.isVisible = false
                it.map { supplier ->
                    suppliersShop.add(supplier) }

                binding.viewLoading.isVisible = false
                binding.viewOrders.isVisible = true

            } else {
                binding.viewLoading.isVisible = false
                binding.fetchStatus.isVisible = true
            }
        })

        if (container != null) {
            containerGlob=container
        }

        return root
    }


    private fun initRecyclerview() {
        adapter = SuppliersAdapter(suppliersShop, navc!!)
        val recyclerView = binding.orderShopRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = adapter
    }

    private fun getSuppliers(container: ViewGroup) {
        val sharedPreference =
            activity?.getSharedPreferences("PREFERENCE_INSHOP_LOGIN", Context.MODE_PRIVATE)!!
        val idShop = sharedPreference.getString("ID", "")!!
        val cm = container.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        suppliersShopViewModel.getSuppliers(idShop,isConnected)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navc= Navigation.findNavController(view)
        initRecyclerview()

        NetworkConnection.getInstance(containerGlob!!.context).observe(viewLifecycleOwner) { isConnected ->
            binding.networkMessage.isVisible = !isConnected
            getSuppliers(containerGlob)
        }

        val spinner = binding.ordersSpinner
        val list = listOf("ALL","ON_REVISION", "ACCEPTED", "DECLINED", "SHIPPING", "COMPLETED")

        if (containerGlob != null) {
            val spinnerAdapter = ArrayAdapter(containerGlob.context,android.R.layout.simple_spinner_item,list)
            spinner.adapter = spinnerAdapter
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    binding.viewLoading.isVisible = true
                    binding.viewOrders.isVisible = false
                    binding.fetchStatus.isVisible = false
                    if(p2>0){
                        val item = spinner.selectedItem.toString()
                        //getOrdersBy(item)
                    }
                    else {
                        getSuppliers(containerGlob)
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    getSuppliers(containerGlob)
                }
            }
        }


//
//        view.findViewById<Button>(R.id.order_Button_Prueba_Products)?.setOnClickListener {
//            val sharedPreference =  context?.getSharedPreferences("PREFERENCE_INSHOP_ORDER", Context.MODE_PRIVATE)
//            val editor = sharedPreference?.edit()
//            editor?.putString("ID_SUPPLIER","IDSupplierName4")
//            editor?.commit()
//            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_suppliers_to_product_suppliers)
//        }

    }

    private fun showError() {

    }

    /**
     * Alert to notify the user from a message.
     */
    @SuppressLint("UseRequireInsteadOfGet")
    private fun showAlert(title: String, message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { _, _ ->
            // do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}