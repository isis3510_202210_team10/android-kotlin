package com.inshop.ui_suppliers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.data_remote.inshop.api.service.RetrofitClient
import com.inshop.cases.SuppliersShopCase
import com.inshop.ui_model.OrderShop
import com.inshop.ui_model.SupplierShop
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class SuppliersViewModel @Inject constructor(
    private val getSuppliersShop: SuppliersShopCase,
) : ViewModel() {
    val supplierShop = MutableLiveData<List<SupplierShop>>()

//    fun getSuppliers(idShop: String) {
//        viewModelScope.launch(Dispatchers.IO) {
//
//            val suppliersList = mutableListOf<SupplierShop>()
//            val instance = RetrofitClient.INSTANCE.fetchSuppliers()
//            val response = instance.body()
//            if (instance.isSuccessful) {
//                for (supplier in response!!) {
//                    val supplierUI = SupplierShop(
//                        supplier._id, supplier.name, supplier.email,
//                        supplier.address, supplier.phone, supplier.companyName
//                    )
//                    suppliersList.add(supplierUI)
//                }
//                supplierShop.postValue(suppliersList)
//            } else {
//                supplierShop.postValue(emptyList<SupplierShop>())
//            }
//        }
//    }

    fun getSuppliers(idShop: String, isConnected: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {

            val ordersList = mutableListOf<SupplierShop>()
            val response = getSuppliersShop.invoke(idShop, isConnected)

            ordersList.addAll(response)
            supplierShop.postValue(ordersList)

        }
    }

    fun deleted() {
        viewModelScope.launch {
            getSuppliersShop.delete()
        }
    }


}