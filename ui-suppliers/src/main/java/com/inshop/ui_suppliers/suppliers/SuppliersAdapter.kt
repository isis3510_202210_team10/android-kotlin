package com.inshop.ui_suppliers.suppliers

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_model.SupplierShop
import com.inshop.ui_suppliers.R


class SuppliersAdapter(private val ordersList: List<SupplierShop>,private val navc: NavController): RecyclerView.Adapter<SuppliersView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuppliersView {
        val layoutInflater = LayoutInflater.from(parent.context)
        return SuppliersView(layoutInflater.inflate(R.layout.item_supplier, parent, false))
    }

    override fun onBindViewHolder(holder: SuppliersView, position: Int) {
        val item = ordersList[position]
        val context: Context = holder.itemView.context
        holder.render(item, context,navc)
    }

    override fun getItemCount(): Int = ordersList.size


}