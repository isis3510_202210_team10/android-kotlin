package com.inshop.ui_suppliers.suppliers

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.inshop.ui_suppliers.R
import com.inshop.ui_model.SupplierShop

class SuppliersView(view: View): RecyclerView.ViewHolder(view) {

    val id_supplier = view.findViewById<TextView>(R.id.order_item_Date)
    val name = view.findViewById<TextView>(R.id.order_item_Supplier)
    val phone = view.findViewById<TextView>(R.id.order_item_Delivery_Date)
    val address = view.findViewById<TextView>(R.id.order_item_Total)
    val catalog = view.findViewById<TextView>(R.id.order_Button_Map_Carga)

    fun render(supplierShop : SupplierShop, context: Context, navc: NavController) {
        id_supplier.text = supplierShop._id
        name.text = supplierShop.name
        phone.text = supplierShop.phone
        address.text = supplierShop.address

        catalog.setOnClickListener{
            val sharedPreference =  context?.getSharedPreferences("PREFERENCE_INSHOP_SUPPLIER", Context.MODE_PRIVATE)
            val editor = sharedPreference?.edit()
            editor?.putString("ID_SUPPLIER",supplierShop._id)
            editor?.commit()
            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_suppliers_to_product_suppliers)

//            val sharedPreference =  context?.getSharedPreferences("PREFERENCE_INSHOP_ORDER", Context.MODE_PRIVATE)
//            val editor = sharedPreference?.edit()
//            editor?.putString("ID_SUPPLIER",supplierShop._id)
//            editor?.commit()
////            navc?.navigate(com.inshop.ui_lateral_menu.R.id.action_navigation_suppliers_to_product_suppliers)
//
////            val mapIntent = Intent(context, MapActivity::class.java)
////            context.startActivity(mapIntent)
        }
    }
}